"""
This package is intended to be used to process field data, although
it could potentially be used for postprocessing other data too.

It contains both the fudamental classes and functions L{fieldpy.core},
a test suite L{fieldpy.tests} as well as specialised sub-packages for specific
tasks:

 - L{stream_gauging}

Things left to do are listed in todo-index.html

"""

__all__ = ["core"]

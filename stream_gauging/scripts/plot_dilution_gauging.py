#!/usr/bin/python2
"""
Plots the breakthrough curves and the stage-discharge relation. Input is the:
1) -i injection file
2) -c calibration files (enclosed in "")
3) -e several ec files (enclosed in "")
4) -s several stage files (enclosed in "")

Example:
./plot_dilution_gauging.py -i inj.maw -c "cali1.maw cali2.maw" -e "ec1.dat ec2.dat"

Injection file example:

#maw Dilution gauging injections from 13 July 2010
#
# Metadata:
#metadata.experimenter = 'MAW + UM'
#metadata.injection_point = "MAW's injection point"
#metadata.sensor = 'older Garry sensor'
#
# Format:
# time (UTC-7) [time_str], amount (kg) [float]
"2010-07-13 09:07:00", 0.2
"2010-07-13 09:22:45", 0.4
"2010-07-13 11:17:00", 0.4
"2010-07-13 13:16:00", 0.4

For an injection file example run plot_calibrations.py without input.

TODO:
- filtering
"""

# standard libary packages
import sys
import os.path
import optparse # to parse command line options

import pylab as plt

## fieldpy packages:
# add the directory where fieldpy package is located to the search path
sys.path.append(os.path.join('..','..','..'))
# use all absolut imports
from fieldpy.stream_gauging.logged_data import *
from fieldpy.stream_gauging.calibration import *
from fieldpy.stream_gauging.dilution_gauging import *


if __name__=='__main__':
    # settings
    reference_resistor = 10e3 # the reference resistor used in the conductivity probe

    ec_field = 'ec' # which field in ec.data to use

    stage_field = 'stage'  # which field in stage.data to use

    stage_averaging_time = 3600 # over what time the stage should be
                                # average to make the stage-discharge relation
    poly_order = 1 # order of polynomial to make the stage-discharge relation
                   # order=1 seems to be sufficient

    # options parsing
    usage = "\b\b\b\b\b\b\b\b" +__doc__ + "\n\n usage: %prog [options]"
    opts = optparse.OptionParser(usage)
    opts.add_option("-i", "--injection", dest="inj_file", action="store",
                    help="The injection file")
    opts.add_option("-c", "--calibration", dest="cali_files", action='store', 
                    help='The calibration files')
    opts.add_option("-e", "--electic-conductivity", dest="ec_files", action='store', 
                    help='The electric conductivity files')
    opts.add_option("-s", "--stage", dest="stage_files", action='store', 
                    help='The stage files')
    (options, args) = opts.parse_args()
    inj_file = options.inj_file
    cali_files = options.cali_files.split()
    ec_files = options.ec_files.split()
    stage_files = options.stage_files.split()    
    #print 'i', inj_file, ' c', cali_files, ' e', ec_files, ' s', stage_files

    # make the stuff
    ec = Conductivity(ec_files, reference_resistor)
    stage = Stage(stage_files, ['time', 'stage'])
    ac = AllCalibrations(cali_files)
    print inj_file, type(inj_file)
    dilg0 = DilutionGauging(inj_file, (0,150), ec, ec_field, ac, stage, stage_field, stage_averaging_time)
    dilg0.plot_all()
    dilg0.make_stage_discharge_relation(verbose_=True, order_of_poly=poly_order)
    
    plt.show()

    # # parse commandline
    # files = sys.argv[1:]
    # if files==[]:
    #     print __doc__
    # else:
        
    #     ac = AllCalibrations(cali_files)        
    #     ac.plot_all()    
    #     plt.show()

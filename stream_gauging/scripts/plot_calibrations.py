#!/usr/bin/python2
"""
Plots the fitting of the conductivity sensor calibration for several
calibrations passed in.  Pass the calibration files in after the command.

Calibration file example:

#maw 1st Conductivity Calibration 13 July 2010
#
# Calibration of the older conductivity sensor in the proglacial river of
# GL1 on 13 July at 10:30.
#
# Note: Times in calibration are not accurate.
#
# Calibration was done by premixing 20g NaCl in 2L of water, to get a 10g/L solution.
# This solution was then added into a 5000mL bucket of water, in the steps written below.
#
# Metadata:
#metadata.experimenter = 'MAW + UM'
#metadata.bucket_size = 5.0
#metadata.bucket_size_units = 'l'
#metadata.calibaration_solution_concentration = 10.0
#metadata.calibaration_solution_concentration_units = 'g/l'
#metadata.sensor = 'older Garry sensor'
#metadata.reference_resistor = 10e3
#metadata.reference_resistor_units = 'Ohm'
#metadata.note = 'Times in calibration are not accurate'
#
# Format:
# time (UTC-7) [time_str], calibration_solution (ml) [float], sensor_readout () [float]
"2010-07-13 10:30:00",0,    0.3030
"2010-07-13 10:31:00",1.25, 0.2965
"2010-07-13 10:32:00",2.5,  0.2890
"2010-07-13 10:33:00",3.75, 0.2815
"2010-07-13 10:34:00",5,    0.2730
"2010-07-13 10:35:00",7.5,  0.2595
"2010-07-13 10:36:00",10,   0.2500
"2010-07-13 10:37:00",15,   0.2320
"2010-07-13 10:38:00",20,   0.2175
"2010-07-13 10:39:00",25,   0.2055
"2010-07-13 10:40:00",30,   0.1945
"2010-07-13 10:41:00",35,   0.1860
"2010-07-13 10:42:00",40,   0.1760
"2010-07-13 10:43:00",45,   0.1685
"2010-07-13 10:44:00",50,   0.1620
"""

# standard libary packages
import sys
import os.path

import pylab as plt

## fieldpy packages:
# add the directory where fieldpy package is located to the search path
sys.path.append(os.path.join('..','..','..'))
# use all absolut imports
from fieldpy.stream_gauging.calibration import *

if __name__=='__main__':
    # parse commandline
    cali_files = sys.argv[1:]
    if cali_files==[]:
        print __doc__
    else:
        ac = AllCalibrations(cali_files)        
        ac.plot_all()    
        plt.show()

"""
Just a few helper classes.
"""
from __future__ import division

import cPickle as pickle
import pprint

import pylab as plt
import numpy as np
import scipy.signal as signal
import scipy.integrate


import fieldpy.core.helper_fns as helper_fns

# try cython imports
try:
    import pyximport; pyximport.install(setup_args={'include_dirs':[np.get_include()]})
except:
    pass

try:
    from fieldpy.core.helper_fns_cython import filter_and_resample
    from fieldpy.core.helper_fns_cython import get_ind_closest
    cython_module = True
except:
    from fieldpy.core.helper_fns import filter_and_resample_slow as filter_and_resample
    cython_module = False

class Data(object):
    """Parent class for all kinds of data.  The data itself should be held in
    self.data as a masked record array (masked np.ma.core.MaskedArray)

    @todo: at the moment only working for 1D data, maybe extend it to N-D
    """
    def __init__(self):
        """
        Sets up the basic structure

        @todo: introduce a way to handle units
        """
        #: where the data is held
        self.data = np.ma.array([])
        #: where the metadata is held
        self.md = Metadata()
        #: get revision of fieldpy package with which this instance was created
        self.md.fieldpy_git_revision = helper_fns.get_current_fieldpy_git_hash()

    def check(self):
        """
        Do integrity checks; should be invoked at the end of __init__
        [NOT IMPLEMENTED]
        """
        pass

    def pickle(self, filename):
        """
        Pickel self.

        @type filename: string
        @param filename: the name of the picklefile (reccomended to use .pkl extension)
        """
        with open(filename, 'wb') as fil:
            pickle.dump(self, fil, protocol=pickle.HIGHEST_PROTOCOL)

    def mask_value(self, field, value):
        """
        Filters out the given value of from the given field by masking them.

        @type field: string
        @param field: the name of the self.data field

        @type value: ?
        @param value: the value which should be masked
        """
        # find value and mask them
        self.data[field][self.data[field]==value] = np.ma.masked

    def mask_jumps(self, field, jump_size):
        """
        Maskes datapoint for which the absolute difference between
        them and the point before is more then jump_size.

        @type field: string
        @param field: the name of the self.data field

        @type jump_size: ?
        @param jump_size: The size of jump which is masked
        """
        absdiff = np.abs(np.diff(self.data[field]))
        self.data[field][absdiff>jump_size] = np.ma.masked

    def mask_if_true(self, field, bool_fn):
        """
        Masks all the values of field where bool_fn is True.

        @type field: string
        @param field: the name of the self.data field

        @type bool_fn: function
        @param bool_fn: function which takes self.data['field'] as argument
                        and returns a boolean
        """
        self.data[field][bool_fn(self.data[field])] = np.ma.masked

    def mask_jumps_one_dir(self, field, jump_size):
        """
        Maskes datapoint for which the difference between them and the
        point before is more/less then jump_size, for jump_size
        positive/negative.

        @type field: string
        @param field: the name of the self.data field

        @type jump_size: ?
        @param jump_size: The size of jump which is filtered
        """
        diff = np.diff(self.data[field])
        jump_size = -jump_size
        if jump_size>0:
            self.data[field][diff>jump_size] = np.ma.masked
        else:
            self.data[field][diff<jump_size] = np.ma.masked

    def filter_by_freq(self, field, output_field, sample_rate, cutoff, bandwidth=None, mode='lowpass'):
        """ Perform filtering with a windowed sinc frequency-domain
        filter. Can be used for either lowpass or highpass filtering,
        useful for removing rapid signal variations or long-wavelength
        trends, respectively.

        The call signature is complicated and should be streamlined.

        @type field: string
        @param field: the name of the self.data field to filter

        @type output_field: string
        @param output_field: the name of the output field in self.data

        @type sample_rate: float
        @param sample_rate: the time or distance per sample, i.e.
        sampling interval

        @type cutoff: float
        @param cutoff: the e-folding cutoff frequency or wavenumber

        @type bandwidth: float
        @param bandwidth: transition bandwidth (narrow means sharper
        response at the expense of a longer filtering kernel and
        slower convolution)

        @type mode: str
        @param mode: either 'lowpass' or 'highpass'
        """

        if bandwidth is None:
            # Guess a reasonable filter bandwidth
            bandwidth = 0.1 * cutoff

        # Compute filter parameters
        bw = bandwidth * sample_rate
        fc = cutoff * sample_rate
        N = 4/bw
        if N % 2 == 0: N += 1

        # Generate the kernel
        kernel = np.sinc(np.arange(-(N-1)/2, (N+1)/2) * fc) \
            * signal.firwin(N, fc, window='blackman')
        kernel = kernel / sum(kernel)

        if mode == 'highpass':
            # Spectral inversion
            kernel = -kernel
            kernel[(N-1)/2] += 1.0

        # Check kernel length
        if len(kernel)>len(self.data[field]):
            raise ValueError(
                "Kernel {0} is longer than input array {1}".format(
                len(kernel), len(self.data[field])))

        # Convolve data field
        filt_data = np.convolve(kernel, self.data[field], mode='same')

        # Add it to data recordarray
        try:
            self.data[output_field] = filt
        except:
            self.data = helper_fns.append_a2mrec(self.data, filt, output_field)
        return

    def filter_low_pass(self, field, output_field,
                        bandwidth, sample_rate, cutoff_freq):
        """
        This implements a sinc filter which is a low pass filter
        (i.e. a step filter in the frequency domain).

        @type field: string
        @param field: the name of the self.data field

        @type output_field: string
        @param output_field: the name of the output field of self.data

        @type bandwidth: float
        @param bandwidth: is the precision of the filter narrower
                          bandwidth would seem better, but increases
                          numerical artefacts.  Set to a few times
                          more than what you wanna filter

        @type sample_rate: float
        @param sample_rate: is the sampling interval

        @param cutoff_freq: float
        @param cutoff_freq: is the centre frequency to cut off at
                            example: you have day-long events and
                            hour-long noise so use a cut-off period of
                            a few hours

        """

        # bandwidth is the precision of the filter
        # narrower bandwidth would seem better, but increases numerical artefacts
        # sample_rate is the sampling interval
        bw = bandwidth * sample_rate

        # cutoff_frequency is the centre frequency to cut off at
        # example: you have day-long events and hour-long noise
        # so use a cut-off period of a few hours
        fc = cutoff_freq * sample_rate

        N = 4//bw
        # Make sure the kernel length is odd
        if N % 2 == 0: N += 1

        #print sample_rate, fc, bw, N

        # Generate the kernel by multiplying a truncated sinc function by some window (blackman is typically used)
        kernel = np.sinc(np.arange(-(N-1)/2, (N+1)/2) * fc) \
                 * signal.firwin(N, fc, window='blackman')
        # normalize to preserve signal amplitudes
        kernel /= kernel.sum()

        if len(kernel)>len(self.data[field]):
            raise ValueError('Kernel is longer than input array: len(kernel)=%i' % len(kernel))

        filt = np.convolve(self.data[field], kernel, mode='same')
        try:
            self.data[output_field] = filt
        except:
            self.data = helper_fns.append_a2mrec(self.data, filt, output_field)


    # def filter_moving_average(self, field, output_field, numtaps, cutoff):
    #     # boxcar filter, should be ok up to 6 sample window
    #     window = 4.
    #     kernel = np.ones(4)/4
    #     out = np.convolve(kernel, self.data[field], mode='same')

class TimeSeries(Data):
    """
    A class to hold time series data.  In self.data['time'] has to be
    the monotonically increasing times.
    """
    def __init__(self):
        super(TimeSeries, self).__init__()

    def check(self):
        super(TimeSeries, self).check()
        self.check_monotonically_inc_time()

    def check_monotonically_inc_time(self):
        """
        Checks whether the times in self.data['time'] are
        monotonically increasing.  Raises an error if they are not.
        """
        if np.any(np.diff(self.data['time'])<=0):
            raise ValueError("self.data['time'] is not stricktly increasing!")

    def check_constant_sampling_interval(self):
        """
        Checks whether sampling interval in self.data['time'] is
        constant.  Raises an error if they are not.
        """
        self.check_monotonically_inc_time()
        samp_int = self.data['time'][1]-self.data['time'][0]
        if np.any(1e-10*samp_int < np.abs(np.diff(self.data['time'])-samp_int)):
            raise ValueError("The sampling interval in self.data['time'] is not constant.")

    def plot_ts(self, var_name=None, ax=None, fmt='-', **plot_kwargs):
        """
        Plots the time series data for var_name.

        @type var_name: string
        @param var_name: string of variable to be plotted

        @type ax: axes object
        @param ax: axes to be plotted into (default: make a new figure)

        @type fmt: string
        @param fmt: format string to be passed to the plotter (default '-')

        @param plot_kwargs: keywords arguments for plt.plot function
        """
        if ax is None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
        if var_name is None:
            var_name = self.data.dtype.names[1]

        ax.plot(self.data['time'], self.data[var_name], fmt, **plot_kwargs)
        ax.set_xlabel('Time')
        ax.set_ylabel(var_name)

        return ax

    def plot_date(self, var_name=None, ax=None, fmt='-', **plot_kwargs):
        """
        Plots the time series data for var_name with plt.plot_date.

        @type var_name: string
        @param var_name: string of variable to be plotted

        @type ax: axes object
        @param ax: axes to be plotted into (default: make a new figure)

        @type fmt: string
        @param fmt: format string to be passed to the plotter (default '-')

        @param plot_kwargs: keywords arguments for plt.plot_date function
        """
        if ax is None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
        if var_name is None:
            var_name = self.data.dtype.names[1]

        # rotate date labels
        labels = ax.get_xticklabels()
        plt.setp(labels, rotation=45)
        ax.plot_date(self.data['time'], self.data[var_name], fmt, **plot_kwargs)
        ax.set_xlabel('Time')
        ax.set_ylabel(var_name)

        return ax

    def time_filter_gauss(self, field, output_field, time_window=0.021):
        """
        Filters the self.data[field] with a weighted moving average filter.

        Appends or over-writes the field output_fild to self.data

        @type field: string
        @param field: the name of the self.data field

        @type output_field: string
        @param output_field: the name of the output field of self.data

        @type time_window: float
        @param time_window: the filter window in units of self.data['time']

        @note: I'm not sure how exactly the missing data are handled...
        """
        filt = helper_fns.filter_gauss(self.data['time'], self.data[field], time_window)
        try:
            self.data[output_field] = filt
        except:
            self.data = helper_fns.append_ma2mrec(self.data, filt, output_field)
        # scikits_imported = True
        # if scikits_imported:
        #     mov_av = scikits.timeseries.lib.cmov_average(self.data[field], window)
        #     self.data = helper_fns.append_ma2mrec(self.data, mov_av, output_field)
        # else:
        #     helper_fns.filter_(self.data['time'], self.data[field], 10./60/24)

    def filter_and_resample(self, field, filter_window,
                            new_sample_int, first_sample=None,
                            modify_instance=False):
        """Filters and resamples a time signal with a running average in a
        given time intervall +/-filter_window/2.  The filter has a PHASE
        SHIFT for a UN-equally sampled signal.

        @param field: which field of self.data to use
        @param filter_window: filter time interval
        @param new_sample_int: the new sampling interval (in units of t)
        @param first_sample: the time of the first sample in the resampled
                             signal. If None (default) it is
                             (approximatly) set to time[0]+filter_window
        @param modify_instance: If true, self.data will only contain
                                the new times and data

        @rtype: list of np.array
        @return: Returns [new_time, new_data]
        """
        [new_t, new_d] = filter_and_resample(self.data['time'], self.data[field], filter_window,
                             new_sample_int, first_sample)
        if modify_instance:
            self.data = np.empty(new_t.shape, dtype=[('time', float),(field, float)])
            self.data['time'] = new_t
            self.data[field] = new_d
            return
        else:
            return new_t, new_d

    def get_ind(self,times):
        """This is the index function you want to use.

        @param times: a list of times

        @returns: a list of indices nearest to the times
        """
        ind = []
        try:
            for ti in times:
                ind.append(self.get_index_nearest(ti)[0])
        except:
            ind.append(self.get_index_nearest(times)[0])
        return ind
    def get_ind_as_slice(self, times):
        """This function takes two times and returns a slice object
        such that the slice includes start and end time.
        """
        ind = self.get_ind(times)
        return slice(ind[0],ind[1]+1)

    def get_index_after(self, time):
        """get_index_after(t) returns the index at t or immediatly
        after t and the exact time corresponding to that index.

        If there is no index afterwards, return (None, None).

        @rtype: tuple
        @return: tuple containing index and the time corresponing to that index
        """
        ind = (np.where(time <= self.data['time']))[0]
        if ind.shape[0] == 0:
            ind = None
            exact_time = None
        else:
            ind = np.min(ind)
            exact_time = self.data['time'][ind]
        return ind, exact_time
    def get_index_before(self, time):
        """get_index_before(t) returns the index at t or immediately
        before t and the exact time corresponding to that index.

        If there is no index before, return (None, None).

        @rtype: tuple
        @return: tuple containing index and the time corresponing to that index
        """
        ind = (np.where(time >= self.data['time']))[0]
        if ind.shape[0] == 0:
            ind = None
            exact_time = None
        else:
            ind = np.max(ind)
            exact_time = self.data['time'][ind]
        return ind, exact_time

    def get_index_nearest(self, time):
        """Get the index and time which is nearest to the time.

        @rtype: tuple
        @return: tuple containing index and the time corresponing to that index
        """
        if cython_module:
            ind = get_ind_closest(self.data['time'], time)
            return ind, self.data['time'][ind]
        else:
            ind_b, t_b = self.get_index_before(time)
            ind_a, t_a = self.get_index_after(time)
            if ind_b is None:
                return ind_a, t_a
            if ind_a is None:
                return ind_b, t_b
            if time-t_b > t_a-time:
                return ind_a, t_a
            else:
                return ind_b, t_b

    def cut_time_series(self, time_span):
        """This cut a piece of the timeseries out and modifies the
        data B{in place}.

        @type time_span:
        """
        sli = self.get_ind_as_slice(time_span)
        self.data = self.data[sli]

    def integrate(self, field, start=None, end=None):
        """
        Integrates the field from start time to end time by a
        trapezoidal method.
        """
        if start is None:
            ind0 = 0
        else:
            ind0 = self.get_index_nearest(start)
        if end is None:
            ind1 = len(self.data)
        else:
            ind1 = self.get_index_nearest(end)

        return scipy.integrate.trapz(self.data[field][ind0:ind1], self.data['time'][ind0:ind1])


class Metadata(object):
    """Just a structure to hold the metadata.
    """
    def __init__(self):
        #: the units of the data
        self.units = []
    def __str__(self):
        return pprint.pformat(self.__dict__)
    def __repr__(self):
        return pprint.pformat(self.__dict__)
    def __eq__(self, other):
        """Compare the two underlying __dict__
        """
        return self.__dict__ == other.__dict__

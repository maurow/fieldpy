% Calibration of the older conductivity sensor in the proglacial river of
% GL1 on 18 July 2010 at 17:36.
%
% Calibration was done by premixing 20g NaCl in 2L of water, to get a 10g/L solution.
% This solution was then added into a 5000mL bucket of water, in the steps written below.
%
% Format:
% mL of 10g/L solution in 5000mL water, sensor readout, concentration in g/L (=kg/m^3)
0,    0.4480, 0
1.25, 0.4255, 2.5e-3
2.5,  0.4060, 5.0e-3
3.75, 0.3917, 7.5e-3
5,    0.3778, 1.0e-2
7.5,  0.3510, 1.5e-2
10,   0.3265, 2.0e-2
15,   0.2895, 3e-2
20,   0.2635, 4e-2

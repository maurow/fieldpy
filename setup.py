"""
Builds the cython stuff. Run with
python2 setup.py build_ext --inplace

or with the script build_cython_extensions.sh
"""

from distutils.core import setup
from distutils.extension import Extension
import numpy
npy_include_dir = numpy.get_include()
# see whether cython is around
try:
    from Cython.Distutils import build_ext
except ImportError:
    use_cython = False
else:
    use_cython = True


cmdclass = { }
ext_modules = [ ]
if use_cython: # build using cython
    ext_modules += [
        Extension("core.helper_fns_cython",
                   ["core/helper_fns_cython.pyx"],
                    include_dirs = [npy_include_dir]
                    )
        ]
    setup(
        cmdclass = {'build_ext': build_ext},
        ext_modules = ext_modules
        )

else: # build just the existing c-function
    ext_modules += [
        Extension("core.helper_fns_cython",
                   ["core/helper_fns_cython.c"],
                    include_dirs = [npy_include_dir]
                    )
        ]
    setup(
        ext_modules = ext_modules
        )


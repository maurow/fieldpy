"""This module is for producing standardized plots for:

 - publications: functions starting with pub_

 - presentations: functions starting with pres_

 - internet: functions starting with net_

 - tools functions: starting with tool_

Generally a function sets the P.rcParams to values suitable for the printing.
 - this is probably not the most clever way of doing things, should
   be done through the API

Some features cannot be done with P.rcParams and can be done with
seprarate functions.

For a description of the sizing of plot and fonts see
http://www.scipy.org/Cookbook/Matplotlib/LaTeX_Examples

Short help about Date ticking
=============================
To set tick locations use::
    ax.xaxis.set_major_locator(plt.DayLocator())
Possible date locators are::
    MinuteLocator: locate minutes
    HourLocator: locate hours
    DayLocator: locate specifed days of the month
    WeekdayLocator: Locate days of the week, eg MO, TU
    MonthLocator: locate months, eg 7 for july
    YearLocator: locate years that are multiples of base
    RRuleLocator: locate using a matplotlib.dates.rrulewrapper. The rrulewrapper is a simple wrapper around a dateutils.rrule (dateutil) which allow almost arbitrary date tick specifications. See rrule example.
    AutoDateLocator: On autoscale, this class picks the best MultipleDateLocator to set the view limits and the tick locations.

More is found here: U{http://matplotlib.sourceforge.net/api/dates_api.html}.
Other non-date locators see: U{http://matplotlib.sourceforge.net/api/ticker_api.html}

And the formatting is done with::
    ax.xaxis.set_major_formatter(plt.DateFormatter('%d'))
where format string is::
    %a      Locale's abbreviated weekday name.
    %A      Locale's full weekday name.
    %b      Locale's abbreviated month name.
    %B      Locale's full month name.
    %c      Locale's appropriate date and time representation.
    %d      Day of the month as a decimal number [01,31].
    %H      Hour (24-hour clock) as a decimal number [00,23].
    %I      Hour (12-hour clock) as a decimal number [01,12].
    %j      Day of the year as a decimal number [001,366].
    %m      Month as a decimal number [01,12].
    %M      Minute as a decimal number [00,59].
    %p      Locale's equivalent of either AM or PM.      (1)
    %S      Second as a decimal number [00,61].      (2)
    %U      Week number of the year (Sunday as the first day of the week)
            as a decimal number [00,53]. All days in a new year preceding
            the first Sunday are considered to be in week 0.      (3)
    %w      Weekday as a decimal number [0(Sunday),6].
    %W      Week number of the year (Monday as the first day of the week)
            as a decimal number [00,53]. All days in a new year preceding
            the first Monday are considered to be in week 0.      (3)
    %x      Locale's appropriate date representation.
    %X      Locale's appropriate time representation.
    %y      Year without century as a decimal number [00,99].
    %Y      Year with century as a decimal number.
    %Z      Time zone characters if no time zone exists).
    %%      A literal "%" character.
"""

import numpy as np
import string
import copy

# from matplotlib import rc
# rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
# #rc('font',**{'family':'serif','serif':['Palatino', 'Computer Modern Roman']})
# rc('text', usetex=True)

import matplotlib.widgets
import matplotlib.font_manager

import pylab as plt


#: in inches: 1 inch = 72.27pt (matplotlib works in inches)
pt_per_inch = 72.27

#: ditto
golden_mean = (np.sqrt(5)-1.0)/2.0         # Aesthetic ratio

#: for dashed lines
dashes = ['--', #    : dashed line
          '-', #     : solid line
          '-.', #   : dash-dot line
          ':'] #    : dotted line


def pub_igs_rcparams():
    """This function is used to set the rcParams for publications in the
    Journal of Glaciology.

    It returns some useful constans:
    @return: column_width, column_height, two_column_width, dashes
    """
    # some dimensions in latex points:
    column_width_pt = 244.6937    #/showthe/columnwidth
    column_width = column_width_pt/pt_per_inch
    text_height_pt  = 724.5       #/showthe/textheight
    column_height = text_height_pt/pt_per_inch
    text_width_pt   = 506.45905   #/showthe/textwidth
    two_column_width = text_width_pt/pt_per_inch


    fig_size_one =  [column_width,column_width*golden_mean]
    fig_size_two =  [two_column_width,two_column_width*golden_mean]
    fig_size_full_hight_one = [column_width,column_height]
    fig_size_full_hight_two = [two_column_width,column_height]

    # figure out what font sizes you want and other parameters which are
    # the same for all size plots:
    font_big = 10
    font_norm = 9
    font_small = 7
    line_thin = 0.1
    line_medium = 0.3
    line_norm = 0.5
    params = {'backend': 'eps',
              'font.size': font_norm,
              'axes.labelsize': font_norm,
              'text.fontsize': font_norm,
              'legend.fontsize': font_norm,
              'legend.linewidth': line_thin,
              'xtick.labelsize': font_small,
              'ytick.labelsize': font_small,
              'lines.linewidth': line_norm,
              'lines.color': 'k',
              'grid.linewidth': line_thin,
              'axes.linewidth': line_medium,
              'font.family': 'sans-serif',
              'font.serif': ['Times New Roman', 'Palatino', 'New Century Schoolbook', 'Bookman', 'Computer Modern Roman'],
              'font.sans-serif': ['Arial', 'FreeSans', 'Bitstream Vera Sans', 'Computer Modern Sans Serif'], #'Free Sans, Arial, Computer Modern Sans serif', #Optima, Helvetica, Avant Garde, Computer Modern Sans serif',
              'font.monospace': ['Courier', 'Computer Modern Typewriter', 'Bitstream Vera Sans Mono'],
              'figure.dpi': 300,
              'savefig.dpi': 100,
              'xtick.major.size': 3,
              'xtick.minor.size': 1.5,
              'ytick.major.size': 3,
              'ytick.minor.size': 1.5,
              'axes.titlesize': font_norm
              }

    plt.rcParams.update(params)
    # use plt.rcdefaults() to restore defaults if needed

#     # parameters which differ for different grapics
#     param_one = {'figure.figsize': fig_size_one}
#     param_two = {'figure.figsize': fig_size_two}
#     param_full_hight_one = {'figure.figsize': fig_size_full_hight_one}
#     param_full_hight_two = {'figure.figsize': fig_size_full_hight_two}


    return column_width, column_height, two_column_width


def pres_rcparams():
    """This function is used to set the rcParams for latex-beamer
    presentations.

    It returns some useful constans:
    @return: column_width, column_height, two_column_width, dashes
    """
    # some dimensions in latex points:
    column_width_pt =  307.28987    #/showthe/columnwidth
    column_width = column_width_pt/pt_per_inch
    text_height_pt  = 200.# 249.80135     #/showthe/textheight
    column_height = text_height_pt/pt_per_inch
    text_width_pt   = 307.28987   #/showthe/textwidth
    two_column_width = text_width_pt/pt_per_inch


    fig_size_one =  [column_width,column_width*golden_mean]
    fig_size_two =  [two_column_width,two_column_width*golden_mean]
    fig_size_full_hight_one = [column_width,column_height]
    fig_size_full_hight_two = [two_column_width,column_height]

    # figure out what font sizes you want and other parameters which are
    # the same for all size plots:
    font_big = 12
    font_norm = 7
    font_small = 6
    line_thin = 1
    line_norm = 2
    params = {'backend': 'png',
              'font.size': font_norm,
              'axes.labelsize': font_norm,
              'text.fontsize': font_norm,
              'legend.fontsize': font_norm,
              'legend.linewidth': line_thin,
              'xtick.labelsize': font_norm,
              'ytick.labelsize': font_norm,
              'lines.linewidth': line_norm,
              'grid.linewidth': line_thin,
              'axes.linewidth': line_thin,
              'font.family': 'sans-serif',
              'font.serif': ['Times New Roman',' Palatino',' New Century Schoolbook',' Bookman',' Computer Modern Roman'],
              'font.sans-serif': ['Arial', 'FreeSans', 'Bitstream Vera Sans', 'Computer Modern Sans Serif'], #'Free Sans, Arial, Computer Modern Sans serif', #Optima, Helvetica, Avant Garde, Computer Modern Sans serif',
              'font.cursive': ['Zapf Chancery'],
              'font.monospace': ['Courier',' Computer Modern Typewriter'],
              'figure.dpi': 300,
              'xtick.major.size': 3,
              'xtick.minor.size': 1,
              'ytick.major.size': 3,
              'ytick.minor.size': 1,
              'savefig.dpi': 300,
              'axes.titlesize': font_norm
              }

    #          'text.usetex': True }
    plt.rcParams.update(params)
    # use plt.rcdefaults() to restore defaults if needed

#     # parameters which differ for different grapics
#     param_one = {'figure.figsize': fig_size_one}
#     param_two = {'figure.figsize': fig_size_two}
#     param_full_hight_one = {'figure.figsize': fig_size_full_hight_one}
#     param_full_hight_two = {'figure.figsize': fig_size_full_hight_two}

    # now do some front checking
    fp = matplotlib.font_manager.FontProperties()
    print fp.get_family(), '\n', matplotlib.font_manager.fontManager.findfont(fp)

    return column_width, column_height, two_column_width



# ----------------------------------------
# helper functions

def finish_up(filename, fig=None, subplot_adjust=None):
    """This function finishes up a plot:
    - adjust the linewidth of the minor ticks
    - adjust the subplots if subplot_adjust is passed
    - it *save* it to the filename. File type is determined by the
      extension.

    It sets the ticks to the width of the axes border
    """
    linewidth = plt.rcParams['axes.linewidth']
    if fig is None:
        fig = plt.gcf()
    axs = fig.axes
    if subplot_adjust is None:
        subplot_adjust = {'left': 0.13,
                       'right': 0.95,
                       'bottom': 0.1,
                       'top': 0.95,
                       'wspace': None,
                       'hspace': None}
    fig.subplots_adjust(**subplot_adjust)
    for ax in axs:
        # there is no get_xticklines methode for the minor ticks (in
        # current matplotlib), so we need to get the list of lines
        # ourselves:
        xminortick_lines=[]
        for minor_ticks in ax.xaxis.minorTicks:
            xminortick_lines.append(minor_ticks.tick1line)
            xminortick_lines.append(minor_ticks.tick2line)
        yminortick_lines=[]
        for minor_ticks in ax.yaxis.minorTicks:
            yminortick_lines.append(minor_ticks.tick1line)
            yminortick_lines.append(minor_ticks.tick2line)
        plt.setp(ax.get_xticklines() + ax.get_yticklines()  +
               xminortick_lines + yminortick_lines, mew=linewidth)

    plt.savefig(filename)

def tool_no_legend_box(ax=None):
    """Removes the legend from the passed list of axes. If no
    arguments are passed, then plt.gcf().axes is used.
    """

    if ax is None:
        ax = plt.gcf().axes
    # this removes the border around the legend labels:
    # setp(legendframe, linewidth=0.0) might also be an option

    for a in ax:
        leg = a.axes.get_legend()
        leg.draw_frame(False)

def tool_touching_subplots(fig=None, top_ticklabels=False):
    """Makes touching subplots and removes the xlabels in all but the
    lowermost subplot.

    @param fig: the figure containing the subplots. defaults to current
                figure.
    @param top_ticklabels: if true also do ticklabels at the top
    """
    if fig is None:
        fig = plt.gcf()
    axs = copy.copy(fig.axes[:-1])
    if top_ticklabels:
        axs[0].xaxis.tick_top()
        axs = axs[1:]
    fig.subplots_adjust(hspace=0)
    for a in axs:
        lab = a.get_xmajorticklabels()
        lab.append(a.get_xminorticklabels())
        xlab = a.xaxis.get_label()
        plt.setp(lab, visible=False)
        plt.setp(xlab, visible=False)

def tool_rotate_xlabels(ax=None, rotation=0):
    """Rotates the xlabels of ax by rotation.
    """
    if ax is None:
        ax = plt.gca()
    labels = ax.get_xticklabels()
    plt.setp(labels, rotation=rotation)
#    plt.setp(labels, verticalalignment='center')


def tool_get_extent(plot_fn, *args, **kwargs):
    """This function returns the extent of the axes when the figure is
    closed. Useful for figuring out the extent on date plots without
    hassel.

    @param plot_fn: a function which does some plotting. does not
                    contain a plt.show() the arguments to plot_fn can
                    be passed in the following *args and **kwargs
    """
    plot_fn(*args, **kwargs)
    xlims = []
    ylims = []
    def resize(event):
        xlims = []
        ylims = []
        for a in plt.gcf().axes:
            xlims.append(a.get_xlim())
            ylims.append(a.get_ylim())
        print xlims, ylims
    resize(None)
    plt.connect('draw_event', resize)
    plt.show()
    return xlims, ylims

def tool_get_coords(plot_fn, ax=None, *args, **kwargs):
    """Returns the x&y coords from where you clicked. Does not respond
    when zooming.

    @param plot_fn: a function which does some plotting. does not
                    contain a plt.show() the arguments to plot_fn can
                    be passed in the following *args and **kwargs
    @param ax: if not not current axes ought to be used. Pass the number of
               the subplot.
    """
    plot_fn(*args, **kwargs)
    xcoord = []
    ycoord = []
    if ax is not None:
        plt.axes(plt.gcf().axes[ax])
    def click(event):
        # this is a trick to let you zoom and pan:
        tb = plt.get_current_fig_manager().toolbar
        # normal event inside axis. return coordinates.
        if event.button==1 and event.inaxes and tb.mode == '':
            print 'x,y = ', event.xdata, event.ydata
            xcoord.append(event.xdata)
            ycoord.append(event.ydata)
    # otherwise it jumps about:
    plt.gca().set_autoscale_on(False)
    #register this function with the event handler
    plt.connect('button_press_event', click)
    # make different cursor
    curs = matplotlib.widgets.Cursor(plt.gca(), color='black', useblit=True)
    plt.show()
    return xcoord, ycoord

def tool_underlay_with_color( x_loc, color='g', axs=None , alpha=0.3):
    """Underlays the axis in the list axs with a color rectangel.

    @param x_loc: (x_min, x_max) the borders of the rectangel
    @param color: 'g' the color
    @param axs: [axes] default [plt.gcf().axes]
    @param alpha: how solid the color is

    @note: on twinx() plots the lines of the first plot will be covered. use
    axs=axs[0:-1:2] when axs is the list of axes.
    """
    if axs is None:
        axs = plt.gcf().axes
    for ax in axs:
        plt.axes(ax)
        ylim = ax.get_ylim()
        xlim = ax.get_xlim()
        x_vertices = [x_loc[0], x_loc[1], x_loc[1], x_loc[0]]
        y_vertices = [ylim[0], ylim[0], ylim[1], ylim[1]]
        plt.fill(x_vertices, y_vertices, facecolor=color, edgecolor=color,
               hold=True)
        #to preserve the limits:
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)

def tool_make_vert_lines( x_loc, axs=None , fmt='k-.'):
    """Makes vertical lines at x_loc.

    @param x_loc: (x_min, x_max) the borders of the rectangel
    @param axs: [axes] default [plt.gcf().axes]
    @param fmt: line style

    @note: on twinx() plots the lines of the first plot will be covered. use
    axs=axs[0:-1:2] when axs is the list of axes.
    """
    if axs is None:
        axs = plt.gcf().axes
    for ax in axs:
        plt.axes(ax)
        ylim = ax.get_ylim()
        xlim = ax.get_xlim()
        y_vertices = np.array([ylim[0], ylim[1]])
        for x in x_loc:
            x_vertices = np.array([x ,x])
            plt.plot(x_vertices, y_vertices, fmt, hold=True)
        #to preserve the limits:
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)

def tool_adjust_ylabels(pos_factor=1, axs=None):
    """Adjust the ylabels such that they align when there are several
    subplots. Only works when the x-Axis are linked.

    Note: it seems that all labels need to be either ascii or
    unicode. a mix leads to misalignment.

    @param pos_factor: controls the horizontal postion. default 1 means
                       same place as outermost label. < 1 more left, > 1
                       more right
    @param axs: a list of axes. default plt.gcf().axes
    """
    # we need to save it in order that the values are right
    plt.savefig('/tmp/tmp_fig.eps')
    if axs is None:
        axs = plt.gcf().axes
    axs_left = [ ax for ax in axs if ax.yaxis.label_position=='left']
    axs_right = [ ax for ax in axs if ax.yaxis.label_position=='right']
    labels_left = [ ax.yaxis.get_label() for ax in axs_left]
    labels_right = [ ax.yaxis.get_label() for ax in axs_right]
    xlims = axs[0].get_xlim()
    loop_over = [x for x in [(axs_right, labels_right),(axs_left, labels_left)] if x!=([],[])]
    for axs, labels in loop_over:
        # get largest x-postion in identity coordates
        # use ax.transAxes.inverse_xy_tup() to go to axes coord
        x_pos = [ lab.get_position()[0] for lab in labels]
        x_position = min(x_pos)  * pos_factor
        y_position= [ lab.get_position()[1] for lab in labels]
        text = [ lab.get_text() for lab in labels]
        fontsize = [ lab.get_fontsize() for lab in labels]
        fontproperties = [ lab.get_font_properties() for lab in labels]
        for n,ax in enumerate(axs):
            # make the original ones disappear
            plt.setp(labels[n], visible=False)
            pos = ax.transAxes.inverted().transform((x_position,y_position[n]))
            # and set new ones.  for some strange reason i need only to
            # take the x_pos transformed and not the y_pos??!
            t = ax.text(pos[0], y_position[n], text[n], transform=ax.transAxes, rotation=90,
                    va='center', ha='center', fontsize=fontsize[n])
            #t.set_fontproperties = fontproperties[n]
            #, ha='center') this seem to have something to do with wonkey text alignement
        #print        t.get_horizontalalignment()
#     print 'Text font:     ' + t.get_fontname()
#     print 'Text size: ' + str(t.get_fontsize())
#     print 'Xlabel font:     ' + plt.gca().xaxis.get_label().get_fontname()
#     print 'Xlabel size:     ' + str(plt.gca().xaxis.get_label().get_fontsize())
#     print 'XTickLabel font: ' + plt.gca().get_xticklabels()[0].get_fontname()
#     print 'XTickLabel size: ' + str(plt.gca().get_xticklabels()[0].get_fontsize())


def tool_label_subplots(axs=None, system='abc', pos='out_right', check_axes=True, start_value=0,
                        fontsize=None):
    """This function labels all the subplots in a figure. The labels are

    @param axs: axes of the subplots to act on. the order of the list
                determines the order of the labeling.
    @param system: labeling system: default 'abc', 'ABC'
    @param pos: default: outside on the right (out_right), in_top_right or (x,y) in
                    figure coordinates.
    @param check_axes: if only one axes, then don't label. default:True
    """

    if axs is None:
        axs = plt.gcf().axes
        if len(axs)==1:
            return
    if fontsize is None:
        fontsize = 12 #plt.rcParams['text.fontsize']#axs[-1].xaxis.get_majorticklabels()[0].get_fontsize()
    if system=='abc':
        labels = string.ascii_lowercase[start_value:]
    elif system=='ABC':
        labels = string.ascii_uppercase[start_value:]
    if pos=='out_right':
        pos_v = (1.02, 0)
    elif pos=='in_top_right':
        pos_v = (0.96, 0.9)
    elif pos=='in_bottom_left':
        pos_v = (0.04, 0.1)
    else:
        pos_v = pos
    for n, ax in enumerate(axs):
        ax.text(pos_v[0], pos_v[1], labels[n], transform=ax.transAxes, fontsize=fontsize)

def tool_contour_remove_litte_ones(cs, num_points):
    """Removes small contours with less than num_points.

    @param cs: is a contour instance
    @param num_points: min number of points
    """
    for i, col in enumerate(cs.collections):
        segs = col._segments
        col.set_segments([seg for seg in segs if len(seg) > num_points])


def tool_set_y_extent_and_ticks_of_subplots(y_extents, y_ticks, axs=None, minor=True):
    """Sets the extents and ticks according to the entires in the corresponding lists.

    @param y_extents: a list of y-extent tuples

    @type y_ticks: list of lists containing B{floats}
    @param y_ticks: the location of the yticks given for each axes.

    @param axs: a list of axis (default: all axis in current figure)

    @param minor: not sure
    
    @note:
     - the y_ticks need to be floats!
     - set minor=False if using plt.twiny
    """
    if axs is None:
        axs = plt.gcf().axes
    for n,ax in enumerate(axs):
        if minor:
            ax.yaxis.set_minor_locator(
                plt.MultipleLocator(np.diff(y_ticks[n])[0]/2))
        ax.set_yticks(y_ticks[n])
        ax.set_ylim(y_extents[n])



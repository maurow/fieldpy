* Want
stage -> discharge

* Need
- [ ] stage filter
- [X] calibration file reader
  - read file
  - make a function relating sensor reading to concentration
  - if there are several calibrations for a day then average them
    (maybe weighted?)
- [ ] dilution experiments reader and such
  - read a dilution file
- [ ] combine stage and dilution exps
- [ ] stage discharge relation

* Dependencies
numpy
dateutil

* Questions
- why do some TOA5 files have a data type in the 5th header line and
  some not?

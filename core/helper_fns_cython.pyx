#cython: embedsignature=True 
"""
A collection of useful helper functions which are written in cython.


So far just:
 - filter_and_resample
 - find closest index of a value in monotoincally increasing vector

@note: for the doctests to run this module has to be compiled with:
       python2 setup.py build_ext --inplace
"""
from __future__ import division
import numpy as np
cimport numpy as np
cimport cython
from libc.math cimport round
from libc.math cimport fabs

ctypedef unsigned int uint

@cython.boundscheck (False) # not needed
cpdef filter_and_resample(np.ndarray[np.float64_t, ndim=1] time,
                          np.ndarray[np.float64_t, ndim=1] data,
                          double filter_window, double new_sample_int, first_sample=None):
    """
    Filters and resamples a time signal with a running average in a
    given time intervall +/-filter_window/2.  The filter has a PHASE
    SHIFT for a UN-equally sampled signal.  Accepts masked arrays but
    not "structured arrays" or "recarrays".


    @param time: the times of the timeseries
    @param data: the data of the timeseries
    @param filter_window: filter time interval
    @param new_sample_int: the new sampling interval (in units of time)
    @param first_sample: the time of the first sample in the resampled
                         signal. If None (default) it is
                         (approximatly) set to time[0]+filter_window

    @rtype: list of np.array
    @return: Returns [new_time, new_data]

    @note: 1) ignores any masks in the time variable.
           2) will not work if it finds no values within filter window
    @todo: Correct above point 2) (as well in helper_fns.py)
    
        >>> tim = np.linspace(-3,90,1000); data = tim**2
        >>> filter_window = 30; new_sample_int = 30
        >>> filter_and_resample(tim, data, filter_window, new_sample_int)
        (array([ -0.,  30.,  60.,  90.]), array([   62.98183318,   975.90237234,  3678.22848073,  6826.19355542]))
        >>> data = np.ma.MaskedArray(data)
        >>> filter_and_resample(tim, data, filter_window, new_sample_int)
        (array([ -0.,  30.,  60.,  90.]), array([   62.98183318,   975.90237234,  3678.22848073,  6826.19355542]))
        >>> data[1:10] = np.ma.masked
        >>> filter_and_resample(tim, data, filter_window, new_sample_int)
        (array([ -0.,  30.,  60.,  90.]), array([   65.73049119,   975.90237234,  3678.22848073,  6826.19355542]))
    """
    cdef int ismasked = 0
    cdef np.ndarray[np.uint8_t, ndim=1] raw_mask  # cdef statment needs to be at uppermost level
    if np.ma.isMaskedArray(data):
        if not issubclass(np.bool_, type(data.mask)): # second statment: there is no masked values
            raw_mask = np.array(data.mask, dtype=np.uint8, copy=False)
            ismasked = 1

    filter_window = filter_window/2.
    if first_sample is None:
        first_sample = time[0] 
        # make it such that it lies nicely with the sampling interval
        first_sample = (round(first_sample/new_sample_int) ) * new_sample_int
        if first_sample < time[0]:
            first_sample = first_sample + new_sample_int
    cdef np.ndarray[np.float64_t, ndim=1] new_t = np.arange(
        first_sample, time[-1]+1e-10*time[-1], new_sample_int)
    cdef np.ndarray[np.float64_t, ndim=1] new_d = np.zeros_like(new_t)

    ## a little study of masked arrays
    # cdef int ii
    # cdef double ss = 0.
    # for ii in range(len(time)):    
    #     ss += data[ii]
    # print ss  # masks are ignored here
    # print data.sum() # masks work fine

    cdef int ind1 = 0  # cannot use uint because of tests inside get_ind_closest
    cdef int ind2 = 0
    cdef int ind1_off = 0
    cdef int ind2_off = 0
    cdef uint ii, jj # Py_ssize_t is prefered over int for loops over arrays: --> but cannot be optimised as potentially negative
    cdef double out, samples
    cdef uint len_v = len(time)

    for ii in range(len(new_t)):
        ind1 += 3*ind1_off//4 # new guess
        ind2 += 3*ind2_off//4 # new guess
        ind1_off = get_ind_closest_faster(time, len_v, new_t[ii]-filter_window, &ind1)
        ind2_off = get_ind_closest_faster(time, len_v, new_t[ii]+filter_window, &ind2)
        if ind1==-1 or ind2==-1:
            raise(ValueError('Index not found'))
        ## note that the standard way of getting the mean is slow because of slicing
        #new_d[ii] = data[ind1:ind2+1].mean()
        # instead we hand write it for 200x speedup
        if ismasked:
            out = 0.
            samples = 0.
            for jj in range(ind1,ind2+1):
                if not raw_mask[jj]: # speedup of ~200x compared to using data.mask[jj]:
                    out += data[jj]
                    samples += 1.
            if out==0:  # if all the samples are 0
                new_d[ii] = 0
            else:
                new_d[ii] = out/samples
        else:
            # fast but doesn't work for ma
            for jj in range(ind1, ind2+1):
                new_d[ii] += data[jj]
            if out==0: # if all the samples are zero
                new_d[ii] = 0
            else:
                new_d[ii] = new_d[ii]/(ind2+1-ind1)
    return new_t, new_d


@cython.boundscheck(False) # not needed
cdef int get_ind_closest_faster(np.ndarray[np.float64_t, ndim=1] vec, uint len_v, 
                         double value, int* ind):
    """
    Finds the index (ind) of vec such that vec[ind]-value is smallest.  Only
    works reliably for monotone vectors.  As start value it uses the
    value found in ind[0].

    This function is somewhat faster than get_ind_closest but cannot
    be called directly from Python (due to the pointer).

    @param vec: the vector, usually a time
    @param len_v: length of vec
    @param value: the value which is compared to vec
    @param ind: the pointer holding the starting guess index and in
                which the result will be stored.

    @return: The number of indices away from inital guess the index was found.

    @note: The algorithm is fairly dum, something like bisection might
           work much faster.  However, for it's current use it is
           probably fine-ish.
    """
    # checks
    if ind[0] < 0:
        ind[0] = 0
    if ind[0]>len_v-1:
        ind[0] = len_v-1
    # figure out sign of the difference between vec[ind[0]] and value
    cdef int si = sign(vec[ind[0]] - value)

    # do a while loop until the sign changes
    cdef int iii = 0  # cannot be uint as I do guess_ind+iii
    cdef int s1 = si
    cdef int s2 = si
    cdef double d1, d2
    cdef uint tmpind
    for iii in range(0,len_v):
        if ind[0]+iii<len_v:
            s1 = sign(vec[ind[0]+iii] - value)
        if ind[0]-iii>=0:
            s2 = sign(vec[ind[0]-iii] - value)
        # break if sign changes:
        if s1!=si:
            tmpind = ind[0] + iii
            d1 = fabs(vec[tmpind] - value)
            d2 = fabs(vec[tmpind-1] - value)
            if d1<d2:
                ind[0] = tmpind
            else:
                ind[0] = tmpind-1
            return iii
        if s2!=si:
            tmpind = ind[0] - iii
            d1 = fabs(vec[tmpind] - value)
            d2 = fabs(vec[tmpind+1] - value)
            if d1<d2:
                ind[0] = tmpind
            else:
                ind[0] = tmpind+1
            return -iii
        # stop if no index has been found and return 0 or len_v
        # depending on which is closer to ind[0]
        if ind[0]+iii>=len_v and ind[0]-iii<0:
            if abs(ind[0]-<int> len_v) < abs(ind[0]):  # cast is necessary
                ind[0] = len_v-1
            else:
                ind[0] = 0
            return 0 


@cython.boundscheck(False) # not needed
def get_ind_closest(np.ndarray[np.float64_t, ndim=1] vec, 
                    double value, guess_ind=None):
    """
    Finds the index (ind) of vec such that vec[ind]-value is smallest.
    Only works reliably for monotone vectors.  As start value it uses
    the value given in guess_ind.
    
    This function is somewhat slower than get_ind_closest but can be
    called directly from Python.  (When calling from cython use
    get_ind_closest_faster)

    @param vec: the vector, usually a time
    @param value: the value which is compared to vec
    @param guess_ind: a guess for index around which the search
    (symmetric) will be started.
    @return: The index

    @note: The algorithm is fairly dum, something like bisection might
    work much faster.

        >>> tim =np.linspace(-3,100,1000)
        >>> get_ind_closest(tim, 49.2)
        506
        >>> get_ind_closest(tim, 49.2, 400)
        506
    """
    cdef uint len_v = len(vec)
    # checks
    if guess_ind is None:
        guess_ind = len_v//2
        if guess_ind < 0:
            guess_ind = 0
        if guess_ind>len_v-1:
            guess_ind = len_v-1
    # figure out sign of the difference between vec[guess_ind] and value
    cdef int si = sign(vec[guess_ind] - value)

    # do a while loop until the sign changes
    cdef uint iii = 0
    cdef int s1, s2
    s1 = si
    s2 = si
    cdef double d1, d2
    cdef uint ind
    while True:
        if guess_ind+iii<len_v:
            s1 = sign(vec[guess_ind+iii] - value)
        if guess_ind-iii>=0:
            s2 = sign(vec[guess_ind-iii] - value)
        # break if sign changes
        if s1!=si:
            ind = guess_ind + iii
            d1 = fabs(vec[ind] - value)
            d2 = fabs(vec[ind-1] - value)
            if d1<d2:
                return ind
            else:
                return ind-1
        if s2!=si:
            ind = guess_ind - iii
            d1 = fabs(vec[ind] - value)
            d2 = fabs(vec[ind+1] - value)
            if d1<d2:
                return ind
            else:
                return ind+1
        # stop if no index has been found and return 0 or len_v
        # depending on which is closer to guess_ind
        if guess_ind+iii>=len_v and guess_ind-iii<0:
            if abs(guess_ind-<int> len_v) < abs(guess_ind):  # cast is necessary
                return len_v-1
            else:
                return 0
        iii += 1

        
@cython.boundscheck(False)     # not needed
cdef inline int sign(double xx):
    """
    Sign function
    """
    if xx>0:
        return 1
    elif xx==0:
        return 0
    else:
        return -1

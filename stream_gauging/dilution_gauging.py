#!/usr/bin/env python2
"""
This file contains the necessary stuff to calculate the stage
discharge relationship for a particular day when dilution experiments
were done.

Workflow:
 - make input files:
  - dilution_exps*.maw: the file specifying all the used input files
  - calibration_*.maw: file with the calibration information
  - injections_*.maw: file with the injection information
 - run make_stage_discharge_relation:
  - check you're happy with the calibrations which will pop-up as
    plots
  - check you're happy with the stage-discharge relation which will
    pop-up as plots
 - use the returned stage-discharge function to turn the stage
   timeseries into discharge timeseries
"""
import copy

import matplotlib.mlab as mlab
import pylab as plt

from fieldpy.core.experiment_classes import *

class SaltInjection(LoggedData):
    """
    Class to hold a single salt injection.
    """
    def __init__(self, inj_file, inj_number, time_interval,
                 conductivity, conductivity_field,
                 all_calib, stage, stage_field, stage_averaging_time=3600):
        """
        After initialisation execute self.post_init to do:
         - self.make_concentration
         - self.calc_discharge
         - self.get_stage

        @type inj_file: sting
        @param inj_file: A file containing information about salt
                         injections, formatted like
                         test_data/injections_13JUL10.maw

        @type inj_number: int
        @param inj_number: The index of the injection wrt above file

        @type time_interval: tuple of floats or None

        @param time_interval: The time interval (measured in secs
                              after injection) used for the calcuation
                              of discharge and such.  If set to None
                              then (0, 240) is used.

        @type conductivity: L{conductivity.Conductivity}
        @param conductivity: An instance holding the conductivity data
                             spanning the experiment time.

        @type conductivity_field: string
        @param conductivity_field: The field of conductivity.data to use


        @type all_calib: L{calibration.AllCalibrations}
        @param all_calib: Instance holding the calibration data and
                          associated conductivity to

        @type stage: L{fieldpy.stream_gauging.stream_gauging.Stage}
        @param stage: Instance holding the stage

        @type stage_field: string
        @param stage_field: The field of stage.data to use

        @type stage_averaging_time: float
        @param stage_averaging_time: the time (in secs) over which to
                                     average the stage. (default 3600)
        """
        super(SaltInjection, self).__init__()

        self.filename = inj_file
        self.inj_data, self.raw_data, self.md = raw_file_readers.read_maw_file(self.filename)
        self.md.inj_number = inj_number
        self.md.inj_time = self.inj_data['time'][inj_number]
        self.md.inj_amount = self.inj_data['amount'][inj_number]
        

        if time_interval is None:
            self.md.time_interval = (0., 240.)
        else:
            self.md.time_interval = time_interval
            
        #: the calcualted discharge
        self.md.discharge = None
        #: the averaged stage
        self.md.stage = None
        self.md.stage_field = stage_field
        self.md.stage_averaging_time = stage_averaging_time

        self._datetime = plt.num2date(self.md.inj_time)

        self.md.conductivity_field = conductivity_field
        self._conductivity_instance = conductivity
        self._all_calib_instance = all_calib
        self._stage = stage

        # fill the self.data with a cut out piece of the conductivity data
        self.data = copy.deepcopy(self._conductivity_instance.data)
        self.data = helper_fns.mrec_keep_fields(self.data, ['time', self.md.conductivity_field])
        self.cut_out_ec_data()

        self.check()

    def post_init(self):
        """
        Run after initialisation.

        @todo: figure out a way to handle the make_concentration arguments...
        @todo: figure out a way to handle the get_stage arguments...
        """
        self.make_concentration()
        self.calc_discharge()
        self.get_stage()

    def check(self):
        """
        Does integrity checks:
         - check the value for the reference_resistor are the same in
           the conductivity instance and all_calib instance
        """
        super(SaltInjection, self).check()
        ref_r1 = self._conductivity_instance.md.reference_resistor
        ref_r2 = self._all_calib_instance.calibrations[0].md.reference_resistor
        if ref_r1!=ref_r2:
            raise ValueError('The reference_resistor used in the Conductivity instance is not equal with the one in the AllCalibrations instance.')
    def cut_out_ec_data(self):
        """
        Cuts out a piece of the filtered conductivity data lp_ec in
        self._conductivity_instance.  Fills self.data with
         - time
         - lp_ec: low pass filtered electict conductivity

        Uses self.md.time_interval for the cut interval

        @todo: adjust here once we got units implemented
        """
        conversion_fac = 1./24./3600.
        start_time = self.md.inj_time + self.md.time_interval[0]*conversion_fac
        end_time = self.md.inj_time + sum(self.md.time_interval) *conversion_fac
        self.cut_time_series([start_time, end_time])

    def make_concentration(self, bckg_aver_window=4, verbose_=True,
                           stage2dis_method='closest'):
        """
        Appends a field 'conc' to self.data containing the salt
        concentration derived from the L{calibration.AllCalibrations}
        instance.

        @type bckg_aver_window: int
        @param bckg_aver_window: The size of the window (in samples)
                                 over which to calulate an average of
                                 the conductivity background.

        @note:
         - there is no provisions if the background at the
           beginning of the trace differs much from at the end. Though,
           this should not happen as the salt cloud passes quickly.
         - there should be a way to choose which conductance2concentration
           function to use
        """
        self.md.background1 = sum(self.data[self.md.conductivity_field][0:bckg_aver_window])/bckg_aver_window
        self.md.background2 = sum(self.data[self.md.conductivity_field][-bckg_aver_window:])/bckg_aver_window
        self.md.background = 0.5*(self.md.background1+self.md.background2)
        if verbose_:
            print 'Backgrounds: ', self.md.background1, self.md.background2, self.md.background
        if stage2dis_method=='closest':
            conc = self._all_calib_instance.conductance2concentration_closest(self.data['time'],
                                              self.data[self.md.conductivity_field], self.md.background, verbose_=True)
        elif stage2dis_method=='average':
            conc = self._all_calib_instance.conductance2concentration_average(self.data['time'],
                                              self.data[self.md.conductivity_field], self.md.background, verbose_=True)
        else:
            raise TypeError('Unknown stage2dis_method')
                
            
        try:
            self.data = helper_fns.append_a2mrec(self.data, conc, 'concentration')
        except:
            self.data['concentration'] = conc

    def calc_discharge(self):
        """
        Calculates the discharge by dividing the injected mass by the
        integrated concentration.  Stores it in self.md.discharge
        """
        conversion_factor = 24*3600
        self.md.discharge = self.md.inj_amount / (self.integrate('concentration') *conversion_factor)
        
    def get_stage(self):
        """
        Averages the stage over averaging_time window (in hours) and
        puts it into self.md.average_stage and self.md.stage_averaging_time.
        """
        averaging_time = self.md.stage_averaging_time
        dt2 = averaging_time/2./24/3600.

        sli = self._stage.get_ind_as_slice([self.md.inj_time-dt2, self.md.inj_time+dt2])
        self.md.average_stage = self._stage.data[self.md.stage_field][sli].mean()

    def plot_breakthrough():
        pass
        

class DilutionGauging(ExperimentTimeSeries):
    """
    Class for the dilution gauging experiments of one day, with the
    aim of making a stage discharge relation for that day.
    """
    def __init__(self, inj_file, time_intervals, conductivity, conductivity_field,
                 all_calib, stage, stage_field, stage_averaging_time=3600):
        """
        After initialistaion exacute:
         - self.make_concentration
         - self.calc_discharge
         - self.get_stage

        @type inj_file: sting or None
        @param inj_file: A file containing information about salt
                         injections, formatted like
                         test_data/injections_13JUL10.maw
                         If None an essentially empty instance will be created.

        @type time_intervals: (list of) tuple of floats or None
        @param time_intervals: The list of time intervals (measured in secs
                              after injection) used for the calcuation
                              of discharge and such.  If set to None
                              then (0, 240) is used.

        @type conductivity: L{fieldpy.stream_gauging.conductivity.Conductivity}
        @param conductivity: An instance holding the conductivtiy data
                             spanning the experiment time.

        @type all_calib: L{fieldpy.stream_gauging.calibration.AllCalibrations}
        @param all_calib: Instance holding the calibration data and associated
                          conductivity to 

        @type stage: L{fieldpy.stream_gauging.stream_gauging.Stage}
        @param stage: Instance holding the stage

        @type stage_field: string
        @param stage_field: The field of stage.data to use

        @type stage_averaging_time: float
        @param stage_averaging_time: the time (in secs) over which to
                                     average the stage. (default 3600)
        """
        super(DilutionGauging, self).__init__()
        if inj_file is None:
            # do nothing
            return
        self.filename = inj_file
        self.data, self.raw_data, self.md = raw_file_readers.read_maw_file(self.filename)

        self._date = plt.num2date(np.round(self.data['time'][0]))
        #: number of injections
        self.md.n_inj = len(self.data)
        if time_intervals is None:
            time_intervals = [(0.,240.) for ii in range(self.md.n_inj)]
        elif type(time_intervals)==type(()):
            time_intervals = [time_intervals for ii in range(self.md.n_inj)]

        # make all salt injections
        self.injections = []
        dis = []
        stages = []
        for ii in range(self.md.n_inj):
            self.injections.append(
                SaltInjection(self.filename, ii, time_intervals[ii],
                              conductivity, conductivity_field, all_calib,
                              stage, stage_field, stage_averaging_time))
            self.injections[-1].post_init()
            dis.append(self.injections[-1].md.discharge)
            stages.append(self.injections[-1].md.average_stage)

        # append the stage and discharge to self.data
        self.data = mlab.rec_append_fields(self.data, 'discharge', np.array(dis))
        self.data = mlab.rec_append_fields(self.data, 'stage', np.array(stages))

        self.make_stage_discharge_relation()
        self.check()

    def check(self):
        """
        Does integrity checks:
         - nothing so far
        """
        super(DilutionGauging, self).check()
        
    def make_stage_discharge_relation(self, order_of_poly=2, verbose_=False):
        """
        Makes the stage discharge relation by fitting a polynomial to
        the stage and discharge record.

        @type order_of_poly: int
        @param order_of_poly: the order of the polynomial

        @param verbose_: if True
        """
        self.md.stage_dis_coef,residuals, rank, singular_values, rcond \
           = np.polyfit(self.data['stage'], self.data['discharge'],
                        order_of_poly, full=True)
        if verbose_:
            print 'Fitting a stage-discharge relation\n  Coef: %s\n  residuals: %s\n  rand: %s\n  singular values: %s\n  condition number: %s' \
                  % (str(self.md.stage_dis_coef), str(residuals), str(rank), str(singular_values), str(rcond))
            ax = self.plot_stage_discharge_relation()
            ax.hold(True)
            self.plot_stage_discharge_measurements(ax)
        
    def plot_stage_discharge_relation(self, ax=None, color='b'):
        """
        Does as advertised.
        """
        if ax is None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
        ax.plot(self.data['stage'], self.data['discharge'], color+'x', label='Measured')
        ax.set_xlabel('Stage (m)')
        ax.set_ylabel('Discharge (m^3/s)')
        ax.legend()
        return ax

    def plot_stage_discharge_measurements(self, ax=None, color='b'):
        if ax is None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
        stage = np.linspace(np.min(self.data['stage']), np.max(self.data['stage']), 30)
        dis = self.stage2dis(stage)
        ax.plot(stage, dis, '-'+color, label='Fit')
        ax.set_xlabel('Stage (m)')
        ax.set_ylabel('Discharge (m^3/s)')
        ax.legend()
        return ax
        
    def stage2dis(self, stage):
        """
        Converts an array of stage data into discharge.

        @param stage: array of stages

        @return: discharge
        """
        return helper_fns.polyval(self.md.stage_dis_coef, stage)

    def return_stage2dis_fn(self):
        """
        Returns a stage to discharge function which can be used
        without the DilutionGauging instance.
        """
        coefs = self.md.stage_dis_coef
        return lambda stage: helper_fns.polyval(coefs, stage)

    def plot_all(self):
        """
        Plot the breakthrough curves of all injections.
        """
        plt.figure()
        for ii,inj in enumerate(self.injections):
            ax = plt.subplot(self.md.n_inj, 1, ii+1)
            title = self.raw_data['time'][ii-1]
            inj.plot_ts('concentration', ax,label=title)
            plt.legend()
            #to plot the time from injection start
            #plt.legend((title))
            

class Stage2Discharge(ExperimentData):
    """
    Class for holding all the dilution gauging experiments of one
    season, with the aim of providing stage discharge relations for a
    season.

    Similar in intent to L{calibration.AllCalibrations} in the sense
    that it unifies several L{dilution_gauging.DilutionGauging}
    instances.

    It will provide several stage to discharge functions:
     - L{stage2dis_all_fn}
    """
    def __init__(self, list_of_dilutiongauging_inst, plot_yes=False):
        """Initialises the class with
        
        @type list_of_dilutiongauging_inst: list of L{dilution_gauging.DilutionGauging}
                                            instances

        @param list_of_dilutiongauging_inst: A list of L{dilution_gauging.DilutionGauging}
                                             instances which will be used to make an overall
                                             stage2discharge relation
        
        @type plot_yes: boolean
        @param plot_yes: Plot the stage-discharge relationships if True (default = False)

        @todo: specify order of polyon for stage2dis
        """
        super(Stage2Discharge, self).__init__()
        self.dilution_gaugings = list_of_dilutiongauging_inst
        self._plot_yes = plot_yes
        # sort them according to when they were done
        cmpfn = lambda x, y : cmp(x._date, y._date)
        self.dilution_gaugings.sort(cmp=cmpfn)
        self.check()
        
    def check(self):
        """
        No checks so far
        """
        super(Stage2Discharge, self).check()

    def plot_comparsion(self):
        """
        plot a comparsion of all stage discharge relation
        """
        dg = self.dilution_gaugings[0]
        ax = dg.plot_stage_discharge_relation()
        dg.plot_stage_discharge_measurements(ax)
        colors = ['r', 'g', 'm', 'k']
        for ii,dg in enumerate(self.dilution_gaugings[1:]):
            dg.plot_stage_discharge_relation(ax, colors[ii])
            dg.plot_stage_discharge_measurements(ax, colors[ii])

    def stage2dis_all_fn(self, stage, dilution_gaugings_to_average):
        """
        Returns a function which converts the stage into a discharge
        using a fit to all stage discharge mesurements.

        @type stage: numpy array or float
        @param stage: the stage to convert into discharge

        @type dilution_gaugings_to_average: list of int
        @param dilution_gaugings_to_average: List of which gaugings of
                                        self.dilution_gaugings to use

        @rtype: numpy array or float
        @return: discharge
        """
        # average the self.calibrations.md.ar coefficient
        c2a = calibrations_to_average
        coef_aver = c2a[0]
        for caa in c2a[1:]:
            coef_aver += self.calibrations[caa].md.stage_dis_coef
        coef_aver = coef_aver/len(c2a)
        # make an empty DilutionGauging instance
        dil2use = DilutionGauging(None, None, None, None, None)
        # and fill in the averaged coefficients
        dil2use.md.stage_dis_coef = coef_aver
        if verbose_:
            print 'Used coefficient is' % str(coef_aver)
        return dil2use.stage2dis(stage
)

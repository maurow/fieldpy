"""
This file contains classes to hold the logged data:
 - stage measurements
 - conductivity measurements

Nothing fancy is done here, as all this is done in 

"""
import numpy as np
import datetime
import pylab as pl
import matplotlib.mlab as mlab
from scipy import polyval, polyfit
# try:
#     import scikits.timeseries.lib
#     scikits_imported = True
# except: # this is to try an avoid having too many depenecies
#     scikits_imported = False

from fieldpy.core.experiment_classes import *
import fieldpy.core.helper_fns as helper_fns
from fieldpy.stream_gauging.calibration import SingleCalibration

class Stage(CampbellCr1000):
    """
    Class to hold and process stage data.

    self.data will have the following 
    """
    def __init__(self, filenames, given_headers=['time', 'stage']):
        """
        Class to hold and process stage data.

        @type filenames: list of strings
        @param filenames: a list of TOA5 Campbell Cr1000 files in
                          chronological order with the same data format.

                          
        @type given_headers: list of stings
        @param given_headers: A list of header stings to be used instead
                              of the ones given in the file header.

        @note:
         - Error checking is just marginal.
         -  Masking record arrays seems a bit tricky here is what I found:
           - U{http://thread.gmane.org/gmane.comp.python.numeric.general/34100/focus=34125}
           - but third creation method here seems to work:
             U{http://docs.scipy.org/doc/numpy/reference/maskedarray.generic.html#using-numpy-ma}

        >>> from fieldpy.core.stream_gauging.stream_gauging import *
        >>> dir_ = 'test_files/stream_gauging/'
        >>> fielns = ['TOA5_stage_1.dat','TOA5_stage_2.dat','TOA5_stage_3.dat']
        >>> stage = Stage([dir_ + fl for fl in fielns], ['time', 'stage'])
        """
        super(Stage, self).__init__(filenames, given_headers)
        self.check()
    def apply_all_filters(self):
        """
        Applies all filters in the right order.
        """

        self.mask_value(field='stage', value=0.)
        self.mask_jumps(field='stage', jump_size=0.15)
        self.mask_
        sample_rate = self.data['time'][1]-self.data['time'][0]
        bandwidth = sample_rate * 1000
        cutoff_freq = 1./(4*sample_rate)
        self.filter_low_pass('stage', 'lp_stage', bandwidth, sample_rate,  cutoff_freq)
        
        # self.time_filter_gauss(field='stage', output_field='stage_filtered',
        #                         time_window=time_window)

    def set_stage2discharge_method(self, method):
        """
        Set a method to use to convert stage to discharge.
        """
        pass
    def check(self):
        super(Stage, self).check()

class Conductivity(CampbellCr1000):
    """Class which holds conductivity experiment data, well actually
    the recorded data is a voltage ratio.
    """
    def __init__(self, filenames, reference_resistor=10e3):
        """Class which holds conductivity experiment data, well actually
        the recorded data is a voltage ratio.  Fills self.data with
         - time
         - V_ratio: the ratio of excitation to measured voltage: this
           is what the datalogger records.
         - ec: is the V_ratio converted into a conductance (not
           conductivity! for this the sensor would have to be
           calibrated with a standard solution)
         
        
        @type filenames: list of strings
        @param filenames: a list of TOA5 Campbell Cr1000 files in
                          chronological order with the same data format.

        @type reference_resistor: float

        @param reference_resistor: The resistance of the reference
                                   resistor used in the experiment in
                                   Ohm.  Note that for dilution
                                   gauging it is not essential to use
                                   the correct value for this but it
                                   is essential to use the same as in
                                   the calibration file. (default=1)
                          
        @note:
         - Error checking is just marginal.

        >>> from fieldpy.core.stream_gauging.stream_gauging import *
        >>> dir_ = 'test_files/stream_gauging/'
        >>> fielns = ['TOA5_EC_1.dat','TOA5_EC_2.dat']
        >>> ec = Conductivity([dir_ + fl for fl in fielns])
        """
        given_headers= ['time', 'V_ratio']
        super(Conductivity, self).__init__(filenames, given_headers)
        
        self.md.reference_resistor = reference_resistor
        self._singlecalibration_instance = SingleCalibration()
        self._singlecalibration_instance._datetime = None
        self._singlecalibration_instance.md.reference_resistor = reference_resistor

        self.make_condctivity()
        self.check()
        

    def apply_all_filters(self, jump_one_dir=0.01):
        #self.mask_jumps_one_dir('ec', jump_one_dir)

        sample_rate = self.data['time'][1]-self.data['time'][0]
        bandwidth = sample_rate * 1e9
        cutoff_freq = 1/(4*sample_rate)
        self.filter_low_pass('ec', 'lp_ec', bandwidth, sample_rate,  cutoff_freq)

    def make_condctivity(self):
        cond = self._singlecalibration_instance.readout2conductance(self.data['V_ratio'])
        self.data = helper_fns.append_a2mrec(self.data, cond, 'ec')
        

#!/usr/bin/env python2
"""
This script analyses the stream site data of 2010 and serves as an
example on how to use the fieldpy.stream_gauging library.

Read the doc in ../__init__.py for a description of the steps used
below.
"""

# standard libary packages
import sys
import os.path

# numpy, scipy and such
import numpy as np
import pylab as plt

# Input data
# ==========

# General
# -------
plot_yes = False # if you wanna plot
from_pickle = False # if you want to load it from the pickle file
save_to_pickle = False # if you want to save to the pickle file

# parent dir to which all directories will be relative
parent_dir = '.' #'fieldpy/stream_gauging/example_stream_site_2010/'  # (use the os.path modul to manipulate paths to make it portable to windows)

# Stage
# -----
stage_pkl_file = 'stage.pkl'
stage_files = [os.path.join('18Jul2010', '1', 'TOA5_5216.stage.dat'),
               os.path.join('18Jul2010', '2', 'TOA5_5216.stage.dat'),
               os.path.join('18Jul2010', '3', 'TOA5_5216.stage.dat'),
               os.path.join('24Jul2010', 'TOA5_5216.stage.dat')]
stage_filter_window = 1./24
stage_resample_time = 1./24/6

# EC
# --
ec_pkl_file = 'ec.pkl'
EC_files = [os.path.join('13Jul2010', 'TOA5_5216.EC.dat'),
            os.path.join('18Jul2010', '2', 'TOA5_5216.EC.dat'),
            os.path.join('18Jul2010', '3', 'TOA5_5216.EC.dat'),
            os.path.join('24Jul2010', 'TOA5_5216.EC.dat')]
reference_resistor = 10e3

# Calibration
# -----------
cali_files = [os.path.join('13Jul2010', 'calibration_13JUL10.maw'),
              os.path.join('18Jul2010', 'calibration_18JUL10_1.maw')]
#              os.path.join('18Jul2010', 'calibration_18JUL10_2.maw')]

# Dilution gauging
# ----------------
injections_file = [os.path.join('13Jul2010', 'injections_13JUL10.maw'),
                   os.path.join('18Jul2010', 'injections_18JUL10.maw')]

stage_averaging_time = 3600 # over how much time stage is averaged to get
                            # the stage-discharge relation
stage_field = 'stage' # the stage.data field which is used for the stage-discharge relation
ec_field = 'lp_ec' # the ec.data field which is used for the stage-discharge relation         
poly_order = 1 # order of line used to make the stage-discharge relation

# for each injection file a time window is needed:
time_windows =  [(0,150), [(0,150), (0,120), (0,100), (0,100), (0,100), (0,100), (0,80), (0,100)]]

# Actual script
# =============
# probably change should be necessary below

## fieldpy packages:
# add the directory where fieldpy package is located to the search path
sys.path.append(os.path.join('..','..','..'))
# use all absolut imports
from fieldpy.stream_gauging.logged_data import *
from fieldpy.stream_gauging.calibration import *
from fieldpy.stream_gauging.dilution_gauging import *
from fieldpy.stream_gauging.discharge import *
import fieldpy.core.helper_fns

# setup


#1 Stage
#=======

if not from_pickle:
    stage_files = [os.path.join(parent_dir, sf) for sf in stage_files]
    stage = Stage(stage_files, ['time', 'stage'])
    stage.mask_value('stage', 0.0)
    stage.mask_jumps('stage', 0.15)
    bool_fn = lambda x: x > 3.0
    stage.mask_if_true('stage', bool_fn)
    stage.filter_and_resample('stage', stage_filter_window, stage_resample_time, modify_instance=True)

    #stage.time_filter_gauss('stage', 'stage_gauss', time_window=1./24)
    if save_to_pickle:
        stage.pickle(os.path.join(parent_dir, stage_pkl_file))
    # filter stage some more...
else:
    stage = helper_fns.unpickle(os.path.join(parent_dir, stage_pkl_file))

if plot_yes:
    ax1 = stage.plot_date('stage')


#2 Conductivity
#==============

if not from_pickle:
    EC_files = [os.path.join(parent_dir, sf) for sf in EC_files]
    ec = Conductivity(EC_files, reference_resistor)
    bool_fn = lambda x: x < 0.00002
    ec.mask_if_true('ec', bool_fn)
    ec.apply_all_filters()
    if save_to_pickle:
        ec.pickle(os.path.join(parent_dir, ec_pkl_file))
else:
    ec = helper_fns.unpickle(os.path.join(parent_dir, ec_pkl_file))

if plot_yes:
    ax = ec.plot_date('lp_ec')
    ax.hold(True)
    ec.plot_date('ec', ax)


#3 Calibration
#=============
cali_files = [os.path.join(parent_dir, sf) for sf in cali_files]
ac = AllCalibrations(cali_files)
if plot_yes:
    ac.plot_all()

#4 Dilution gauging
#==================
injf0 = os.path.join(parent_dir, injections_file[0])
injf1 = os.path.join(parent_dir, injections_file[1])

dilg0 = DilutionGauging(injf0, time_windows[0], ec, ec_field, ac, stage, stage_field, stage_averaging_time)
dilg0.plot_all()
dilg0.make_stage_discharge_relation(order_of_poly=poly_order)

dilg1 = DilutionGauging(injf1, time_windows[1], ec, ec_field, ac, stage, stage_field, stage_averaging_time)
dilg1.plot_all()
dilg1.make_stage_discharge_relation(order_of_poly=poly_order)

#5 stage2discharge
#=================
s2d = Stage2Discharge([dilg0, dilg1], plot_yes=True)

s2d.plot_comparsion()

#6 the discharge
#===============
stage2dis = dilg1.return_stage2dis_fn()
dis = Discharge(stage, stage_field, stage2dis)

if plot_yes:
    dis.plot_date('discharge')
    plt.show()

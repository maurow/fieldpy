#!/usr/bin/python2
"""
Plots stage measurements.

Pass in a list of stage files.

ToDo: - allow filtering
"""

# standard libary packages
import sys
import os.path

import pylab as plt

## fieldpy packages:
# add the directory where fieldpy package is located to the search path
sys.path.append(os.path.join('..','..','..'))
# use all absolut imports
from fieldpy.stream_gauging.logged_data import *

if __name__=='__main__':
    # parse commandline
    stage_files = sys.argv[1:]
    if stage_files==[]:
        print __doc__
    else:
        stage = Stage(stage_files, ['time', 'stage'])
        stage.plot_date('stage')
        plt.show()

"""
Holds the parent classes for classes to evaluate experiments.
"""
import matplotlib.mlab as mlab
import pylab as plt
import numpy as np

import fieldpy.core.raw_file_readers as raw_file_readers
from fieldpy.core.extra_classes import Metadata, Data, TimeSeries
import fieldpy.core.helper_fns as helper_fns

class LoggedData(TimeSeries):
    """Parent class for all kinds of logged data, be it by hand or by
    logger (i.e. time series).  So, LoggedData are looked at as
    simple, i.e. there is only very minimal data processing to be done
    to get the data, definitely not a compound of several experiments,
    use L{ExperimentData}.
    """
    def __init__(self):
        super(LoggedData, self).__init__()

    def check(self):
        """
        Does integrity checks, should be invoced at the end of
        __init__ of the children classes.

        @todo: check that units are given.
        """
        super(LoggedData, self).check()


class ExperimentData(Data):
    """Parent class for all kinds of experiments which cannot easily
    be represented as time series.  For some experiments it will
    conincide with L{LoggedData} but mostly it will be processed from
    the logged data, e.g. filtered, pieced together, etc.
    """
    def check(self):
        """
        Does integrity checks, should be invoced at the end of __init__

        @todo: check that units are given.
        """
        super(ExperimentData, self).check()

class ExperimentTimeSeries(TimeSeries):
    """Parent class for all kinds of experiments which are time
    series.  For some experiments it will conincide with L{LoggedData}
    but mostly it will be processed from the logged data,
    e.g. filtered, pieced together, etc.
    """
    def check(self):
        """
        Does integrity checks, should be invoced at the end of __init__

        @todo: check that units are given.
        """
        super(ExperimentTimeSeries, self).check()


class CampbellCr1000(LoggedData):
    """Holds Cr1000 data.
    """

    def __init__(self, filenames, given_headers=[]):
        """
        Class to hold logged data from a Cr1000 datalogger.  Can take
        several files of the same table.

        @type filenames: list of strings
        @param filenames: a list of TOA5 Campbell Cr1000 files in
                          chronological order with the same data format.


        @type given_headers: list of stings
        @param given_headers: A list of header stings to be used instead
                              of the ones given in the file header.

        @note: Error checking is just marginal.

        >>> from fieldpy.core.experiment_classes import *
        >>> dir_ = 'test_files/'
        >>> fielns = ['TOA5_n1.dat','TOA5_n2.dat','TOA5_n3.dat']
        >>> c1000 = CampbellCr1000([dir_ + fl for fl in fielns], ['time', 'stage'])
        """
        super(CampbellCr1000, self).__init__()
        self.filenames = filenames
        self.given_headers = given_headers

        datas = []
        self.raw_datas = []
        mds = []

        # read all the files
        for filename in filenames:
            data, raw_data, md = raw_file_readers.read_campbell_TAO5(filename, given_headers)
            datas.append(data)
            self.raw_datas.append(raw_data)
            mds.append(md)

        if self.given_headers==[]:
            self.t_name = 'TIMESTAMP'
        else:
            self.t_name = self.given_headers[0]

        self.make_time(datas)
        self.make_data(datas)

    def make_time(self, datas):
        """
        Pieces together the time row and returns an error if wrong.

        @type datas: a dict of list
        @param datas: as generated in L{__init__}
        """
        time = datas[0][self.t_name]
        for data in datas[1:]:
            time = np.append(time, data[self.t_name], 0)

        # # check that it is the same for all other tables
        # for table in datas.keys()[1:]:
        #     time2 = datas[table][0][self.t_name]
        #     for data in datas[table][1:]
        #         time2 = np.append(time2, data[self.t_name], 0)
        #     if not np.all(time==np.array(time2)):
        #         raise ValueError('The TIMESTAMP is not equal in all given tables. Error found in table %s' % table)

        # make a record array
        self.data = np.zeros(time.shape, dtype=[('time', float)])
        self.data['time'] = time
        self.check_monotonically_inc_time()

        # make a masked record array
        self.data = self.data.view(np.ma.MaskedArray)

    def make_data(self, datas):
        """
        Fills the rest of self.data
        """
        for recname in datas[0].dtype.names:
            if recname==self.t_name:
                continue
            tmp = datas[0][recname]
            for data in datas[1:]:
                tmp = np.append(tmp, data[recname], 0)
            self.data = helper_fns.append_a2mrec(self.data, tmp, recname)



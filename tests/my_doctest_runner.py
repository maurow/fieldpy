#!/usr/bin/env python2
# c.f. http://docs.python.org/library/doctest.html
from __future__ import absolute_import
from __future__ import division


import sys
sys.path.append('../..')

if __name__ == "__main__":
    _verbose = False
    _report = True
    import doctest
    from fieldpy.core import raw_file_readers, experiment_classes, helper_fns, extra_classes
    # doctest.testmod(fieldpy)
    doctest.testmod(raw_file_readers, report=_report, verbose=_verbose)
    doctest.testmod(experiment_classes, report=_report, verbose=_verbose)
    doctest.testmod(helper_fns, report=_report, verbose=_verbose)
    try:
        from fieldpy.core import  helper_fns_cython
    except:
        print 'Not testing helper_fns_cython due to import error'
    else:
        doctest.testmod(helper_fns_cython, report=_report, verbose=_verbose)
    # from fieldpy.stream_gauging import *
    # doctest.testmod(calibration, report=_report, verbose=_verbose)

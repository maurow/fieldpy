#!/usr/bin/python2
"""
Plots electic conductivity measurements.

Pass in a list of conductivty files.

ToDo: - allow filtering
"""

# standard libary packages
import sys
import os.path

import pylab as plt

## fieldpy packages:
# add the directory where fieldpy package is located to the search path
sys.path.append(os.path.join('..','..','..'))
# use all absolut imports
from fieldpy.stream_gauging.logged_data import *

if __name__=='__main__':
    # parse commandline
    ec_files = sys.argv[1:]
    if ec_files==[]:
        print __doc__
    else:
        reference_resistor = 10e3
        ec = Conductivity(ec_files, reference_resistor)
        ec.plot_date('ec')
        plt.show()

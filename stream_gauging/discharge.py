"""
In this module the class 
"""

import matplotlib.mlab as mlab
import pylab as plt

from fieldpy.core.experiment_classes import *

class Discharge(TimeSeries):
    """
    Well, this class holds the discharge time series
    """

    def __init__(self, stage, stage_field, stage2discharge_fn):
        """
        
        """
        super(Discharge, self).__init__()
        self.stage = stage
        self.stage2discharge = stage2discharge_fn
        self.data = stage.data
        self.md.stage_field = stage_field
        dis = self.stage2discharge(self.data[stage_field])
        try:
            self.data = helper_fns.append_ma2mrec(self.data, dis, 'discharge')
        except:
            self.data = mlab.rec_append_fields(self.data, 'discharge', dis)
        self.check()

    def check(self):
        super(Discharge, self).check()
        

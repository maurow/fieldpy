# Scope

This is a software library intended to be used to process scientific
data, in particular field data.  It is somewhat outdated as it was
written before I knew of [Pandas](http://pandas.pydata.org/).

# Documentation

Can be found [here](http://maurow.bitbucket.org/docs/fieldpydoc/),
with a
[detailed example](http://maurow.bitbucket.org/docs/fieldpydoc/fieldpy.stream_gauging-module.html)
on how to do dilution stream gauging.

# Dependencies
Required:
- python 2.7
- numpy
- scipy
- matplotlib

Optional
- ipython (for ease of use)
- epydoc (for building the documentation)
- cython (for faster filters and maybe more)

To be added in the future:
- [netcdf](https://www.unidata.ucar.edu/software/netcdf/)
- [netcdf4-python](https://code.google.com/p/netcdf4-python)/

# ToDo

## Bugs
- .maw files use fixed length strings instead of object-type
## Features
- leave time objects as datetime.datetime objects as matplotlib can
  handle it OR better wait for numpy datetime64 type and support from
  matplotlib for it
- implement netcdf reader and writer


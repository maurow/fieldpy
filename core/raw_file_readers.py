#!/usr/bin/env python2
"""
This file contains a collection of file reader functions which can be
called when reading in the raw data files.  The datafiles must be in a
array (1D or 2D) format.

The functions must have the following calling signature:
reader_fun(input_file, other_parameters).

The time (in matplotlib format) will has always to be in the first column.

And will return a tuple (data, raw_data, metadata).  The data will
have the format: time (in matplolib format), other (non-time related)
data columns.
"""

from __future__ import division

import numpy as np
import pylab as plt
import datetime
import copy
import matplotlib.mlab as mlab
from distutils.version import LooseVersion

import pdb

# my modules
from fieldpy.core.extra_classes import Metadata

################################
# first a few helper functions
################################

def readfile_raw(input_file, separator=None, comment=None,
                   start=0, stop=-1, ignore_empty=False):
    """Reads a text file line by line and returns the raw data into
    the nested list raw_data.  Ignores all trainling empty lines.

    @type input_file: string
    @param input_file: The file to read.

    @type separator: string
    @param separator: Column separator in file, if equals to 'no_split' colums will not be split.

    @type comment: string
    @param comment: a string which comments the rest of the line

    @type start: int
    @param start: which line to start on (default 0)

    @type stop: int
    @param stop: which line to stop on (default -1, i.e. to the end)

    @type ignore_empty: boolena
    @param ignore_empty: if C{True}, ignore empty lines

    @rtype: list
    @return: Returns a nested list containing the split raw file lines (as strings).

    >>> readfile_raw('test_files/maw_file_test.maw', separator=',', comment='#')
    [['2010-07-13 08:49:00', '0', '0.3030', '5', 'asdd asdlkj asl'], ['2010-07-13 08:56:00', '15', '0.2320', '8866', 'asdd asdlkj asl'], ['2010-07-13 08:58:00', '25', '0.2055', '5', '7'], ['2010-07-13 09:03:00', '50', '0.1620', '5', '']]
    """

    raw_data = []
    rw_opt='rU'
    line_no = 0
    with open(input_file, rw_opt) as fil:
        # ignore the first start-lines:
        for i in range(start):
            line = fil.readline()
            line_no += 1
        for line in fil:
            if line_no==stop:
                break
            if comment is not None:
                # middle of line comments
                line = line.split(comment)[0].strip()
                if line=='':
                    continue
            if separator != 'no_split':
                tmp = line.strip()
                if ignore_empty and tmp=='':
                    continue
                tmp = [el.strip() for el in tmp.split(separator)]   # pharse
            else:
                tmp = line.strip()
            raw_data.append(tmp)
            line_no += 1
    # remove empty lines at the end of the file
    while True:
        if raw_data[-1]=='':
            raw_data.pop()
        else:
            break
    return raw_data 

def campbell2num_date(campbell_date):
    """Convert our standart campbell notation into pylab format.

    @type campbell_date: list of lists or numpy array
    @param campbell_date: A numpy array with Campbell dates

    @returns: numpy array

    >>> cd1 = [[2006, 139, 1245]]
    >>> cd2 = [[2006, 139, 1245, 34]]
    >>> campbell2num_date(cd1)
    array([ 732450.53125])
    >>> campbell2num_date(cd2)
    array([ 732450.53164352])
    >>> np.alltrue(cd1 == num_date2campbell(campbell2num_date(cd1)))
    True
    >>> np.alltrue(cd2 == num_date2campbell(campbell2num_date(cd2)))
    False
    >>> np.alltrue(cd2 == num_date2campbell(campbell2num_date(cd2), secs=True))
    True
    """
    if type(campbell_date)!=type(np.array([])):
        campbell_date = np.array(campbell_date)

    # check for non 2-D arrays
    if len(campbell_date.shape)==1 or len(campbell_date.shape)>2:
        raise(TypeError('2-d numpy array or list of lists expected.'))
    
    if campbell_date.shape[1]==3:       # no seconds
        campbell_date = np.hstack(( campbell_date, np.zeros((campbell_date.shape[0],1)) ))

    out = np.zeros(campbell_date.shape[0])

    # not vectorised because of datetime.datetime
    for n,i in enumerate(campbell_date):
        out[n] =( plt.date2num(datetime.datetime(int(i[0]),1,1))  # year
                  + i[1]-1.0                                    # days
                  + np.floor(i[2]/100.0)/24.0                    # hours
                  + (i[2] - np.floor(i[2]/100.0)*100.0)/24.0/60.0 # minutes
                  + i[3]/24./3600                               # seconds
                )
    return out

def num_date2campbell(num_date, secs=False):
    """Convert a numerical date as in pylab to our standart campbell
    notation (in a narray or a single date).

    @type num_date: numpy array or list
    @param num_date: vector of pylab dates
    
    @type secs: boolean
    @param secs: if C{True} secs are appended

    @return: numpy array with rows [year, julian day, time (,seconds)]

    >>> dt = datetime.datetime(2006,6,6,12,37,25)
    >>> nd = plt.date2num(dt)
    >>> num_date2campbell([nd])
    array([[2006,  157, 1237]])
    >>> num_date2campbell([nd], secs=True)
    array([[2006,  157, 1237,   25]])
    """
    if type(num_date)!=type(np.array([])):
        num_date = np.array(num_date)

    # check for non 1-D arrays
    if len(np.squeeze(num_date).shape)>1:
        raise(TypeError('1-d numpy array or a list expected, shape is %s', num_date.shape.__repr__()))

    
    dateobj = plt.num2date(num_date + 0.45/24/60/60)   # 0.5/24/60/60 to avoid rounding errors on minutes and seconds
    out = []
    init_tmp_list = []
    for i in xrange(len(num_date)):
        tmp_list = copy.copy(init_tmp_list)
        tmp_list.append(dateobj[i].year)
        tmp_list.append(int(np.floor(num_date[i]
                                    - plt.date2num(datetime.datetime(dateobj[i].year,1,1)) + 1)))
        if secs: # to round to the nearst minute if no seconds:
            tmp_list.append(int(dateobj[i].hour*100 + dateobj[i].minute))
        else:
            tmp_list.append(int(dateobj[i].hour*100 + dateobj[i].minute + int(np.round(dateobj[i].second/60.))))
        if secs:
            tmp_list.append(int(dateobj[i].second))
        out.append(tmp_list)
    return np.array(out, dtype=int)

def iso_time_to_date(isostrings, method='hash'):
    """
    Converts a ISO 8601 date & time string (well, slightly perverted)
    into a matplotlib date number.  Note that this implementation is
    not particularly fast as it uses several try/except blocks.  If
    efficiency is a concern, hard-code it.

    @type isostrings: list of stings
    @param isostrings: ISO 8601 date & time string: following formats are supported:

    @type method: string
    @param method: Switch to use different alogrithm.
                   In order of decreasing speed:
                    - magic 40x
                    - hash  3x
                    - fast  4x (needs numpy>1.5)
                    - ''    1x
                   Set to '' to get good error checking/reporting.

    @rtype: np.array of floats
    @return: matplotlib date numbers

    >>> iso_time_to_date(["2010-07-07 00:00:00"])
    array([ 733960.])
    >>> iso_time_to_date(["2010-07-07 00:00:00","2010-07-07 00:00:00.5","2010-09-07 03:01:00.5"])
    array([ 733960.        ,  733960.00000579,  734022.12570023])
    >>> iso_time_to_date(["2010-07-07 00:00:00","2010-07-07 00:01:00","2010-09-07 03:01:00"])
    array([ 733960.        ,  733960.00069444,  734022.12569444])
    """
    if not np.iterable(isostrings):
        raise TypeError('isostrings is not a string nor iterable!')

    # ## this is really slow (5x slower than fast method)
    # out = np.zeros((len(isostrings)))
    # for ii,isostr in enumerate(isostrings):
    #     out[ii] = plt.datestr2num(isostr)
    # return out
    if method=='fast': # 50% faster
        tmpar = np.zeros((len(isostrings),7), dtype=int)
        # parse the date string
        for ii,isostr in enumerate(isostrings):
            li = isostr.split('.')
            if len(li)==2: # need to convert into microseconds
                tmpar[ii,-1] = int(float('0.' + li.pop()) * 1e6)
            li = li[0].split(':')
            tmpar[ii,-2] = int(li.pop())
            tmpar[ii,-3] = int(li.pop())
            li = li[0].split(' ')
            tmpar[ii,-4] = int(li.pop())
            li = li[0].split('-')
            tmpar[ii,-5] = int(li.pop())
            tmpar[ii,-6] = int(li.pop())
            tmpar[ii,-7] = int(li.pop())
        # def convert(arr):
        #     # converts array as above into vector of datenums
        #     out = np.ndarray(arr.shape[0])
        #     for ii, vec in enumerate(arr):
        #         out[ii] = plt.date2num(datetime.datetime(*vec.tolist()))
        #     return out
        def convert_fast(arr): # 40% faster than above function
            # converts array as above into vector of datenums
            out = np.ndarray(arr.shape[0])
            # find all the unique year+months
            [yearmonths, ymind] = np.unique(arr[:,0]+arr[:,1]/100., return_inverse=True)
            # and turn them into datenums
            for ii, ym in enumerate(yearmonths):
                year = int(np.floor(ym))
                month = int(round((ym-year)*100))
                yearmonths[ii] = datetime.date(year, month, 1).toordinal()
            # scatter it again
            out = yearmonths[ymind]
            # add days
            out += arr[:,2] - 1
            # add times (copied from matplotlib's dates.py)
            HOURS_PER_DAY = 24.
            MINUTES_PER_DAY  = 60.*HOURS_PER_DAY
            SECONDS_PER_DAY =  60.*MINUTES_PER_DAY
            MUSECONDS_PER_DAY = 1e6*SECONDS_PER_DAY
            out += (arr[:,3]/HOURS_PER_DAY   + arr[:,4]/MINUTES_PER_DAY +
                    arr[:,5]/SECONDS_PER_DAY + arr[:,6]/MUSECONDS_PER_DAY)
            return out


        return convert_fast(tmpar)
    elif method=='hash': # this makes a hash to avoid calling datetime.datetime so often
        out = np.empty(len(isostrings))
        year_month_hash = {}
        # parse the date string and build dict of converted year-months 
        for ii,isostr in enumerate(isostrings):
            # parse
            li = isostr.split(':')
            secs = float(li.pop())
            mins = int(li.pop())
            li = li[0].split(' ')
            hours = int(li.pop())
            li = li[0].split('-')
            days = int(li.pop())
            months = int(li.pop())
            years = int(li.pop())
            # year-mont hash
            yearmonth = years+months/100.
            if not year_month_hash.has_key(yearmonth):
                year_month_hash[yearmonth] = datetime.date(years, months, 1).toordinal()
            # year
            out[ii] = year_month_hash[yearmonth]
            # add days
            out[ii] += days - 1
            # add times (copied from matplotlib's dates.py)
            HOURS_PER_DAY = 24.
            MINUTES_PER_DAY  = 60.*HOURS_PER_DAY
            SECONDS_PER_DAY =  60.*MINUTES_PER_DAY
            out[ii] += (hours/HOURS_PER_DAY   + mins/MINUTES_PER_DAY +
                    secs/SECONDS_PER_DAY)
        return out
    elif method=='magic':  # this works by some numpy magic to vectorise it
        str_len = 30
        # convert to array of strings
        if type(isostrings) == np.ndarray and np.issubdtype(isostrings.dtype, np.dtype('O')):
            isostrings = isostrings.astype('S'+str(str_len))
        elif type(isostrings) == list:
            isostrings = np.array(isostrings,'S'+str(str_len))
        # string array viewn as bytes (charaters)
        isobytes = isostrings.view(np.byte)
        isobytes = isobytes.reshape((isostrings.shape[0], str_len))
        # now this is an array of charaters
        isoints = isobytes - 48 # subtracting 48 from ASCII numbers gives their integer values
        isoints[isoints==-48] = 0 # set empty strings to zero
        # add times (copied from matplotlib's dates.py)
        years = np.sum(isoints[:,0:4]*np.array([1000,100,10,1]),1)
        months = np.sum(isoints[:,5:7]*np.array([10,1]),1)
        years_months = years+months/100.
        # make a hash for all possible year-months between
        # years[0] and years[-1] -> this should be efficient for time series which have many datapoints per month
        year_month_hash = {}
        for year in range(years[0], years[-1]+1):
            for month in range(1,13):
                year_month = year+month/100.
                year_month_hash[year_month] = datetime.date(year, month, 1).toordinal()
        # convert into days (in matplotlib date-format)
        days = np.empty(len(isostrings))
        for k in year_month_hash:
            days[years_months==k] = year_month_hash[k] - 1
        # and the rest is easy
        HOURS_PER_DAY = 24.
        MINUTES_PER_DAY  = 60.*HOURS_PER_DAY
        SECONDS_PER_DAY =  60.*MINUTES_PER_DAY
        days += np.sum(isoints[:,8:10]*np.array([10,1]),1)
        days += 1/HOURS_PER_DAY * np.sum(isoints[:,11:13]*np.array([10,1]),1)
        days += 1/MINUTES_PER_DAY * np.sum(isoints[:,14:16]*np.array([10,1]),1)
        days += 1/SECONDS_PER_DAY * np.sum(isoints[:,17:19]*np.array([10,1]),1)
        if str_len>19:  # have fractional seconds too
            days += 1/SECONDS_PER_DAY * np.sum(isoints[:,20:]*np.logspace(-1, -(str_len-20), 10),1)
        return days
    else: # will catch errors better
        out = []
        for isostr in isostrings:
            try:
                out.append(plt.date2num(datetime.datetime.strptime(isostr, '%Y-%m-%d %H:%M:%S')))
                continue
            except ValueError:
                pass
            try:
                out.append(plt.date2num(datetime.datetime.strptime(isostr, '%Y-%m-%d %H:%M:%S.%f')))
                continue
            except ValueError:
                raise
        return np.array(out)

################################
## READER FUNCTIONS
################################

def read_campbell_cr10x(input_file, headers=None, secs=False, year=None):
    """
    Reads the file in standard Campbell CR10X dataformat:

    number, year, julian day, time, data, ...

    Or if year is not None:
    number, julian day, time, data, ...

    @type input_file: string
    @param input_file: input file name

    @type headers: [string]
    @param headers: a list of headers to be given to the variable columns
                    (default is [var1, var2, var3...])

    @type secs: boolean
    @param secs: If true the fifth row is interpreted as seconds else as data

    @type year: integer
    @param year: If not None, then it is interpreted that the datafile
                 contains no year column and value of 'year' parameter is
                 used.  (note, the colums are counted from zero)

    @rtype: tuple
    @return: tuple (data, raw_data, metadata)

    >>> data, raw_data, metadata = read_campbell_cr10x('test_files/cr10x.dat')
    >>> data, metadata, raw_data # doctest:+ELLIPSIS
    (array([(732102.9722222222, -0.30595, 3.2896, 335.44),
           (732102.9791666667, -0.30629, 3.2656, 332.99),
           (732102.9861111111, -0.27962, 3.2405, 330.43),
           (732102.9930555556, -0.30513, 3.205, 326.81),
           (732103.0, -0.30523, 3.1689, 323.13),
           (732103.0069444445, -0.30457, 3.141, 320.29)], 
          dtype=[('time', '<f8'), ('var0', '<f8'), ('var1', '<f8'), ('var2', '<f8')]), {'headers': ['time', 'var0', 'var1', 'var2'],
     'input_file': 'test_files/cr10x.dat',
     'raw_headers': ['station number',
                     'year',
                     'julian day',
                     'time',
                     'var0',
                     'var1',
                     'var2'],
     'secs': False,
     'units': [],
     'year': None}, array([[  1.05000000e+02,   2.00500000e+03,   1.56000000e+02,
              2.32000000e+03,  -3.05950000e-01,   3.28960000e+00,
              3.35440000e+02],
    ...
    """
    # initialise
    metadata = Metadata()
    metadata.__dict__['input_file'] = input_file
    metadata.secs = secs
    metadata.year = year

    first_line = readfile_raw(input_file, stop=1, separator=',')[0]
    # if no headers are given make a standard one
    header_len = len(first_line)
    if headers is None:
        if secs:
            if year is None:
                head = ['station number', 'year', 'julian day', 'time', 'secs']
            else:
                head = ['station number', 'julian day', 'time', 'secs']
        else:
            if year is None:
                head = ['station number', 'year', 'julian day', 'time']
            else:
                head = ['station number', 'julian day', 'time']
        std_head_len = len(head)
        headers = head + ['var'+str(ii) for ii in range(header_len - std_head_len)]
    else: # otherwise calcualte std_head_len
        # check
        if len(headers)!=len(first_line):
            raise TypeError('Given header does not have the same length as the first row of the file.')
        if secs:
            if year is None:
                std_head_len = 5
            else:
                std_head_len = 4                
        else:
            if year is None:
                std_head_len = 4                
            else:
                std_head_len = 3                
    metadata.raw_headers = headers
    metadata.headers = ['time'] + headers[std_head_len:]

    # read the file:
    raw_data = np.genfromtxt(input_file, delimiter=',')
    tmp_dat = copy.deepcopy(raw_data)

    # convert time
    if year is not None:
        tmp_dat = np.hstack((tmp_dat[:,0], year*np.ones(tmp_dat.shape[0]), tmp_dat[:,1:]))
    if secs:
        last_ind_time = 5
    else:
        last_ind_time = 4
    tmp_t = campbell2num_date(tmp_dat[:,1:last_ind_time])
    tmp_t = tmp_t[:,np.newaxis]
    tmp_dat = np.hstack((tmp_t, tmp_dat[:,last_ind_time:]))

    # figure out the 'data' dtypes (a bit akward due to numpy)
    dtype_data = np.dtype([(head, np.float64) for head in  metadata.headers])

    # create data with a 'view': note that there are some issues with strides thus the ascontiguousarray
    data = np.ascontiguousarray(tmp_dat).view(dtype_data).squeeze()

    return data, raw_data, metadata

def read_campbell_TAO5(input_file, given_headers=[]):
    """
    Reads the file in TAO5 Campbell dataformat as used by CR1000:

    Resources:
    http://www.campbellsci.com/documents/manuals/loggernet_3-1.pdf
    Section B.1.4

    Header format
    file format, station, logger type, serial number, OS version, logger-program file name, logger-program file checksum, table name
    "TIMESTAMP","RECORD",fieldname,fieldname,...
    "TS","RN", field-units, field-units,...
    "","",field recording method,field recording method,...

    If the fieldname is not specified then a header of format 'var1'
    etc will be given, except if specified in the list L{given_headers}.
    
    @type input_file: string
    @param input_file: input file name

    @type given_headers: list
    @param given_headers: list of header names to give in the record
                          array data. If an entry is None then the
                          default one is used. Note that the field
                          'RECORD' is ignored in the data and thus
                          does not feature in this list.

    @rtype: tuple
    @return: tuple (data, raw_data, metadata)

    @note: It is assumed that any string-like thing is a date+time string
    
    >>> d,rd,md = read_campbell_TAO5('test_files/TOA5_cr1000.dat')
    >>> d,rd,md # doctest:+ELLIPSIS
    (array([ (733960.0, 13.72, 12.6, 733959.9930787038, 13.43, 10.2, 733959.5997685185, 4.493, 7),
           (733961.0, 13.78, 12.48, 733960.2569675926, 13.15, 17.09, 733960.6921296297, 4.064, 8),
           (733962.0416666666, 13.74, 12.5, 733961.2257175926, 13.07, 17.36, 733961.6785185186, 5.637, 10)], 
          dtype=[('TIMESTAMP', '<f8'), ('Batt_Volt_Max', '<f8'), ('Batt_Volt_Min', '<f8'), ('Batt_Volt_TMn', '<f8'), ('Batt_Volt_Avg', '<f8'), ('Panel_Temp_Max', '<f8'), ('Panel_Temp_TMx', '<f8'), ('var7', '<f8'), ('Panel_Temp_Avg', '<i8')]), ...
    """
    metadata = Metadata()
    TOA5_info = {}
    TOA5_info_fields_line1 = ['file_format', 'station', 'logger_type', 'serial_number',
                           'OS_version', 'logger-program_file_name',
                           'logger-program_file_checksum', 'table_name']

    header_lines = readfile_raw(input_file, stop=5, separator=',')
    ## process header
    for ii,key in enumerate(TOA5_info_fields_line1):
        try:
            TOA5_info[key] = header_lines[0][ii].strip('"')
        except:
            TOA5_info[key] = header_lines[0][ii]
    TOA5_info['fields'] = [st.strip('"') for st in header_lines[1]]
    TOA5_info['units'] = [st.strip('"') for st in header_lines[2]]
    TOA5_info['recording_type'] = [st.strip('"') for st in header_lines[3]]
    metadata.__dict__['TOA5_info'] = TOA5_info
    metadata.__dict__['input_file'] = input_file
    metadata.raw_units = TOA5_info['units']
    metadata.units = metadata.raw_units[0:1] + metadata.raw_units[2:] 
    # fill metadata
    metadata.headers = []
    metadata.raw_headers = []
    ind = -1
    # check consistency of given_headers if passed in
    if len(given_headers)>0 and len(given_headers)!=(len(TOA5_info['fields'])-1):
        raise ValueError('Variable given_headers is not of right length. It is %i but should be %i' % (len(given_headers), len(TOA5_info['fields'])-1))
    for ii,hd in enumerate(TOA5_info['fields']):
        if hd=="RECORD": 
            metadata.raw_headers.append(hd)
            continue # skip that field for data
        ind += 1
        if len(given_headers)>0 and given_headers[ind] is not None:
            metadata.raw_headers.append(given_headers[ind])
            metadata.headers.append(given_headers[ind])
            continue
        if hd!='':
            metadata.raw_headers.append(hd)            
            metadata.headers.append(hd)
        else:
            head = 'var' + str(ind)
            metadata.raw_headers.append(head)
            metadata.headers.append(head)
    # figure out the datatypes of the data
    first_line = header_lines[-1]
    raw_dtypes = []
    type_dict = {int: 'int',
                 str: 'str',
                 float: 'float'}
    for field in first_line: # eval does the magic thanks to TOA5 quoting all strings
        raw_dtypes.append(type_dict[type(eval(field))])
    metadata.raw_dtypes = raw_dtypes

    ## read the data
    # 1) we want varible length strings in the numpy array so use 'np.object' datatype
    dt = [rdt if rdt!='str' else 'O' for rdt in metadata.raw_dtypes]
    # add the header to the dtype
    dtypes_raw = np.dtype(zip(metadata.raw_headers,dt))
    # 2) make a converter to remove double quotes
    remove_double_quotes = lambda str_: str_.replace('"', '')
    converters = {}
    for ii,dt in enumerate(metadata.raw_dtypes):
        converters[ii] = remove_double_quotes
    # 3) finally read the file
    # (there was an API change in genfromtxt thus this if/else)
    if LooseVersion(np.__version__)<LooseVersion('1.5'):  
        raw_data = mlab.csv2rec(input_file, names=metadata.raw_headers,
                            skiprows=4, delimiter=',').view(np.ndarray)
        # raw_data = np.genfromtxt(input_file, delimiter=',', skiprows=4, 
        #                                  dtype=dtypes_raw, converters=converters)

        ## figure out the 'data' dtypes (a bit akward due to numpy) (forget about record number)
        dtype_data = [raw_data.dtype[ii] for ii in [0]+range(2,len(raw_data.dtype))]
        # we assume that all 'object's are datetimes and will be converted, thus object->float
        dtype_data = [dt if dt!=np.object else np.dtype(np.float) for dt in dtype_data]

        # initialise 'data'
        data = np.zeros(len(raw_data), dtype=zip(metadata.headers, dtype_data))

        #pdb.set_trace()
        # now make the data, i.e. convert time strings into matplotlib date-numbers
        for head in metadata.headers:
            if raw_data.dtype[head]==np.dtype('object'):
                # convert first to datetime then datenum
                data[head] = plt.date2num(raw_data[head])
            else:
                data[head] = raw_data[head]


    else: 
        raw_data = np.genfromtxt(input_file, delimiter=',', skip_header=4, 
                                 dtype=dtypes_raw, converters=converters)

        ## figure out the 'data' dtypes (a bit akward due to numpy) (forget about record number)
        dtype_data = [raw_data.dtype[ii] for ii in [0]+range(2,len(raw_data.dtype))]
        # we assume that all strings are datetimes and will be converted, thus object->float
        dtype_data = [dt if dt!=np.object else np.dtype(np.float) for dt in dtype_data]

        # initialise 'data'
        data = np.zeros(len(raw_data), dtype=zip(metadata.headers, dtype_data))
        # now make the data, i.e. convert time strings into matplotlib date-numbers
        for head in metadata.headers:
            if raw_data.dtype[head]==np.dtype('object'):
                # convert first to datetime then datenum
                data[head] = iso_time_to_date(raw_data[head])
            else:
                data[head] = raw_data[head]

    return data, raw_data, metadata

def read_maw_file(input_file):
    """
    Reads a standart MAW file (as only used by me, Mauro A Werder)
    with format:

    #maw name of dataset
    # comment line
    #metadata is an metadata tag:
    #metadata.eg = 'asdf'
    # will create a attribute in metadata.eg with value 'asdf'
    #metadata.num = '1.234'
    # 
    # the last comment line has the format and will be put into
    # metadata['headers'], metadata['units'] and use as datatype:
    # name0 (units) [dtype], name1 (units) [dtype], name2 (units) [dtype], ...
    val0, val1, val2 ...
    .
    .
    .

    dtypes is one of the following: int, float, str, time_str

    Time is represented as an ISO 8601 sting: "yyyy/mm/dd HH:MM:SS(.FF)"
    excluding the 'T' without time zone information (which should be
    given in the units as eg (UTC-7)).

    The idea is to have a easy to parse text represenation of (a
    subset of) what can be contained in a netcdf3 file.  
    
    @type input_file: string
    @param input_file: input file name

    @rtype: tuple
    @return: tuple (data, raw_data, metadata)

    >>> d,rd,md = read_maw_file('test_files/maw_file_test.maw')
    >>> d,rd,md 
    (array([(733966.3673611111, 0.0, 0.303, 5, 'asdd asdlkj asl'),
           (733966.3722222223, 15.0, 0.232, 8866, 'asdd asdlkj asl'),
           (733966.3736111111, 25.0, 0.2055, 5, '7'),
           (733966.3770833333, 50.0, 0.162, 5, '')], 
          dtype=[('time', '<f8'), ('var1', '<f8'), ('var2', '<f8'), ('var3', '<i8'), ('var4', 'O')]), array([('2010-07-13 08:49:00', 0.0, 0.303, 5, 'asdd asdlkj asl'),
           ('2010-07-13 08:56:00', 15.0, 0.232, 8866, 'asdd asdlkj asl'),
           ('2010-07-13 08:58:00', 25.0, 0.2055, 5, '7'),
           ('2010-07-13 09:03:00', 50.0, 0.162, 5, '')], 
          dtype=[('time', 'O'), ('var1', '<f8'), ('var2', '<f8'), ('var3', '<i8'), ('var4', 'O')]), {'calibaration_solution_concentration': 10.0,
     'calibaration_solution_concentration_units': 'g/l',
     'dtypes': ['time_str', 'float', 'float', 'int', 'str'],
     'experimenter': 'MAW + UM',
     'headers': ['time', 'var1', 'var2', 'var3', 'var4'],
     'raw_headers': ['time', 'var1', 'var2', 'var3', 'var4'],
     'title': 'Test file',
     'units': ['UTC-7', 'ml', '', 'm^3', '']})
    """
    # INITIALISE
    raw_data = readfile_raw(input_file, separator='no_split')
    data = []
    comment = []
    metadata = Metadata()

    # PRE-PARSE FILE
    for line in raw_data:
        if line.startswith('#'): # get rid of comments
            comment.append(line.strip())
        else:
            tmp = line.split(',')
            tmp = [tt.strip(' "') for tt in tmp]
            data.append(tuple(tmp))

    # NOW MAKE THE METADATA STURCTURE
    if not comment[0].startswith('#maw'):
        raise TypeError("File does not start with '#maw'")
    else:
        metadata.title = comment[0].strip('#maw ')
    for line in comment:
        if line.startswith('#metadata.'):
            tmp = line.split('#metadata.')[1]
            tmp = tmp.split('=')
            tmp = [t.strip() for t in tmp]
            # use eval, so basically anything python handles can be written after the =
            if tmp[1]!='nan':
                metadata.__dict__[tmp[0]] = eval(tmp[1])
            else:
                metadata.__dict__[tmp[0]] = float('nan')

    # PARSE LAST COMMENT LINE:
    last_line = [st.strip() for st in comment[-1].strip('#').split(',')]
    headers = []
    units = []
    dtypes = []
    for head in last_line:
        tmp = head.split('(')
        headers.append(tmp[0].strip())
        tmp = tmp[1].split(')')
        units.append(tmp[0].strip())
        dtypes.append(tmp[1].strip(' []'))
        if dtypes[-1]=='':
            raise ValueError('No datatype given in file')
    metadata.__dict__['headers'] = headers
    metadata.__dict__['raw_headers'] = headers    
    metadata.__dict__['units'] = units
    metadata.__dict__['dtypes'] = dtypes
    # the dtype for the time string is just a sting but we use
    # np.object to have varible length strings
    dtypes_raw = [np.dtype(dt) if dt!='time_str'
                  else np.dtype('O') for dt in dtypes]
    # same goes for strings
    dtypes_raw = [np.dtype(dt) if dt!='str'
                  else np.dtype('O') for dt in dtypes_raw]
    # but once converted into a matplotlib datenumber it's a float
    dtypes_processed = [np.dtype(dt) if dt!='time_str'
                        else np.dtype(float) for dt in dtypes]
    # normal strings stay objects
    dtypes_processed = [np.dtype(dt) if dt!='str'
                        else np.dtype('O') for dt in dtypes_processed]

    # PARSE DATA
    # raw_data contains all as it was in the file
    raw_data = np.array(data, zip(headers, dtypes_raw))  # this will convert the stings as specified in dtypes_raw
    # initialise
    data = np.zeros(len(raw_data), dtype=zip(headers, dtypes_processed))
    # now make the data, i.e. convert time strings into matplotlib date-numbers
    for ii, head in enumerate(headers):
        if dtypes[ii]=='time_str':
            data[head] = iso_time_to_date(raw_data[head])
        else:
            data[head] = raw_data[head]

    return data, raw_data, metadata


#####################################
## add your own reader functions here
#####################################


# if __name__=='__main__':
#     import doctest
#     doctest.testmod()
    

"""
A collection of useful helper functions.
"""
import copy
import pdb
import cPickle as pickle
import subprocess
import os

import numpy as np
import matplotlib.mlab as mlab
from scipy.interpolate import splprep, splev
from scipy.signal import gaussian


def pickle_(var, filename):
    """
    Pickels var into filename.

    @param var: the varibale to pickle

    @type filename: string
    @param filename: the name of the picklefile (reccomended to use .pkl extension)
    """
    with open(filename, 'wb') as fil:
        pickle.dump(var, fil, protocol=pickle.HIGHEST_PROTOCOL)

def unpickle(filename):
    with open(filename, 'rb') as fil:
        return pickle.load(fil)

def nonmasked_values(ma):
    """
    Returns an array containing only the non-masked values of a masked
    array and the indices of them.
    """
    return np.asarray(ma.data[~ma.mask]), ~st.mask

def append_field2struct_arr(struct_ar, name, vec, dtype=None):
    """
    Returns a new structured array with the arrays appended. (the
    arrays cannot be masked)

    @param struct_ar: structured array to be appended to

    @type name: string
    @param name: the name of the new datatype

    @type vec: np.ndarray
    @param vec: the vector to be appended (its length has to be == struct_ar.shape[0])

    @param dtype: the dtype to be used (defaults to dtype of arr)

    >>> r = np.zeros((3,), dtype=[('foo', int), ('bar', float)])
    >>> toap = np.array([1,2,3])
    >>> append_field2struct_arr(r, 'new', toap)
    array([(0, 0.0, 1), (0, 0.0, 2), (0, 0.0, 3)], 
          dtype=[('foo', '<i8'), ('bar', '<f8'), ('new', '<i8')])
    """
    # check
    if struct_ar.dtype.names is None:
        raise(TypeError(
            "First argument is not a structured array!"))
    # figure out new dtype
    if dtype is None:
        dtype = vec.dtype
    if dtype.names is not None:
        raise(TypeError(
            "vec or dtype is that of a structured array!"))
    # dtype of new field
    ndtype = [(name, dtype)]
    # dtype of whole thing
    newdtype = np.dtype(struct_ar.dtype.descr + ndtype)
    new_struct_arr = np.ndarray(struct_ar.shape, dtype=newdtype)
    for name_ in struct_ar.dtype.names:
        new_struct_arr[name_] = struct_ar[name_]
    new_struct_arr[name] = vec
    return new_struct_arr


def append_mrec2mrec(mrec, toap_mr):
    """
    Return a new masked structured array with a column appended from
    another masked structured array.

    @type mrec: record np.ma.core.MaskedArray
    @param mrec: the masked record array to which will be appended

    @type toap_mr: record np.ma.core.MaskedArray
    @param toap_mr: record np.ma.core.MaskedArray of the right size to be appended

    @rtype: np.ma.core.MaskedArray
    @return: The masked record array with appended fields

    @note: Modified from
    U{http://mail.scipy.org/pipermail/numpy-discussion/2007-September/029357.html}

    >>> r = np.zeros((3,), dtype=[('foo', int), ('bar', float)])
    >>> mr = r.view(np.ma.MaskedArray)
    >>> mr[0] = np.ma.masked
    >>> toap = np.ones((3,), dtype=[('foobar', int)])
    >>> toap_mr = toap.view(np.ma.MaskedArray)
    >>> toap_mr[2] = np.ma.masked
    >>> append_mrec2mrec(mr, toap_mr)
    masked_array(data = [(--, --, 1) (0, 0.0, 1) (0, 0.0, --)],
                 mask = [(True, True, False) (False, False, False) (False, False, True)],
           fill_value = (999999, 1e+20, 999999),
                dtype = [('foo', '<i8'), ('bar', '<f8'), ('foobar', '<i8')])
    <BLANKLINE>
    """
    # figure out new dtype
    dtype = toap_mr.dtype
    newdtype = np.dtype(mrec.dtype.descr + dtype.descr)
    newdtype_bool = np.dtype([(nt[0], 'bool') for nt in newdtype.descr])
    # make a rec_array with the right data in it
    newcolname = toap_mr.dtype.names[0]
    data = append_field2struct_arr(mrec, newcolname, toap_mr[newcolname])
    # make a new mask
    newmask = append_field2struct_arr(mrec.mask, newcolname, toap_mr[newcolname].mask)
    # creat the new np.ma.core.MaskedArray
    newmrec = np.ma.core.MaskedArray(data, mask=newmask, dtype=newdtype)
    # for field in mrec.dtype.fields:
    #     newmrec[field] = mrec[field]
    # newmrec[name] = toap_mr
    # mrec = newmrec
    return newmrec

def append_ma2mrec(mrec, toap_ma, name):
    """
    Return a new masked record array with a column appended from a
    masked array.  It is assumed that whole records are masked and not
    individual entires in a record.  The mask will the constructed
    with a locial or operation.

    @type mrec: record np.ma.core.MaskedArray
    @param mrec: the masked record array to which will be appended

    @type toap_ma: np.ma.core.MaskedArray (no-record)
    @param toap_ma: np.ma.core.MaskedArray of the right size to be appended

    @type name: string
    @param name: the name to give to the new colum

    @rtype: np.ma.core.MaskedArray
    @return: The masked record array with appended fields

    @note: Modified from
    U{http://mail.scipy.org/pipermail/numpy-discussion/2007-September/029357.html}

    >>> r = np.zeros((3,), dtype=[('foo', int), ('bar', float)])
    >>> mr = r.view(np.ma.MaskedArray)
    >>> mr[0] = np.ma.masked
    >>> toap = np.ones((3,), dtype=int)
    >>> toap_ma = toap.view(np.ma.MaskedArray)
    >>> toap_ma[2] = np.ma.masked
    >>> append_ma2mrec(mr, toap_ma, 'foobar')
    masked_array(data = [(--, --, 1) (0, 0.0, 1) (0, 0.0, --)],
                 mask = [(True, True, False) (False, False, False) (False, False, True)],
           fill_value = (999999, 1e+20, 999999),
                dtype = [('foo', '<i8'), ('bar', '<f8'), ('foobar', '<i8')])
    <BLANKLINE>
    """
    # figure out new dtype
    dtype = toap_ma.dtype
    newdtype = np.dtype(mrec.dtype.descr + [(name, dtype.descr[0][1])])
    newdtype_bool = np.dtype([(nt[0], 'bool') for nt in newdtype.descr])
    # make a rec_array with the right data in it
    newcolname = name
    data = append_field2struct_arr(mrec, newcolname, toap_ma)
    # make a new mask
    newmask = append_field2struct_arr(mrec.mask, newcolname, toap_ma.mask)
    # creat the new np.ma.core.MaskedArray
    newmrec = np.ma.core.MaskedArray(data, mask=newmask, dtype=newdtype)
    # for field in mrec.dtype.fields:
    #     newmrec[field] = mrec[field]
    # newmrec[name] = toap_ma
    # mrec = newmrec
    return newmrec

def append_a2mrec(mrec, toap_a, name):
    """
    Return a new masked record array with a column appended from a
    array.  It is assumed that whole records are masked and not
    individual entires in a record.  The mask will the constructed
    with a locial or operation.

    @type mrec: record np.ma.core.MaskedArray
    @param mrec: the masked record array to which will be appended

    @type toap_a: np.array (no-record)
    @param toap_a: np.array of the right size to be appended

    @type name: string
    @param name: the name to give to the new colum

    @rtype: np.ma.core.MaskedArray
    @return: The masked record array with appended fields

    @note: thhs just wraps append_field2struct_arr

    >>> r = np.zeros((3,), dtype=[('foo', int), ('bar', float)])
    >>> mr = r.view(np.ma.MaskedArray)
    >>> mr[0] = np.ma.masked
    >>> toap = np.ones((3,), dtype=int)
    >>> append_a2mrec(mr, toap, 'foobar')
    masked_array(data = [(--, --, 1) (0, 0.0, 1) (0, 0.0, 1)],
                 mask = [(True, True, False) (False, False, False) (False, False, False)],
           fill_value = (999999, 1e+20, 999999),
                dtype = [('foo', '<i8'), ('bar', '<f8'), ('foobar', '<i8')])
    <BLANKLINE>
    """
    newcolname = name
    dtype = toap_a.dtype
    newdtype = np.dtype(mrec.dtype.descr + [(name, dtype.descr[0][1])])
    data = append_field2struct_arr(mrec, newcolname, toap_a)
    newmask = np.zeros(len(toap_a), dtype=bool)
    newmask = append_field2struct_arr(mrec.mask, newcolname, newmask)
    newmrec = np.ma.core.MaskedArray(data, mask=newmask, dtype=newdtype)
    return newmrec

def mrec_keep_fields(mrec, names):
    """
    Return a new numpy record array with only fields listed in names

    @note: copied and adjusted from mlab.rec_keep_fields
    """
    data = []
    mask = []
    dtype = []
    for name in names:
        data.append(mrec[name])
        mask.append(mrec.mask[name])
        dtype.append((name, mrec[name].dtype))
    data = np.rec.fromarrays(data, names=names)
    mask = np.rec.fromarrays(mask, names=names)
    dtype = np.dtype(dtype)

    newmrec = np.ma.core.MaskedArray(data, mask=mask, dtype=dtype)
    return newmrec

def filter_gauss(t, x, delta_t, sigma_factor=2.):
    """Filtes a time signal with a weighted running average in a given
    time intervall +/-delta_t.
    The weight is a gaussian such that at t+/-delta_t is at
    sigma_factor sigma, e.g sigma_factor=2 is c.a. 1/8 of the weight
    of the center. The filter is symmetric, i.e. no phase shift.

    @param t: time vector
    @param x: data vector (or array)
    @param delta_t: filter time interval

    @note:
     - Really slow as it itterates over all elements... maybe a cython case?
     - Adapted and refined from my old dataprocessing stuff

    >>> t = np.linspace(0,100,10)
    >>> x = np.sin(t)
    >>> filter_gauss(t, x, 10*np.pi)
    array([ 0.        , -0.4581598 , -0.03226275,  0.1324937 ,  0.06281049,
           -0.11801212, -0.09001935,  0.09725727,  0.36815581, -0.50636564])
    >>> tm = t.view(np.ma.MaskedArray)
    >>> xm = x.view(np.ma.MaskedArray)
    >>> xm[3] = np.ma.masked
    >>> filter_gauss(tm, xm, 10*np.pi)
    masked_array(data = [0.0 -0.45815980054253036 -0.03752284419362081 -0.21996490093777407
     0.07305105402354425 -0.3863900563241611 -0.09001934956295403
     0.09725726775687152 0.368155810038066 -0.5063656411097588],
                 mask = [False False False False False False False False False False],
           fill_value = 1e+20)
    <BLANKLINE>
    """
    delta_t = float(delta_t)
    # do a consistency check: 
    if delta_t >= (np.max(t)-np.min(t))/2.:
        raise(TypeError(
            "delta_t is bigger than the time interval of t."))

    if np.ma.is_masked(t):
        raise ValueError('Masking the time dimension is not supported')
    # if x is a row vector, make a column vector:
    row_vec = False
    if len(x.shape) == 1:
        x = x.reshape(len(x),1)
        row_vec = True

    # make a decent kernel
    # use a gauss shape with width such that the outermost point is at 2 sigma:
    def gauss(xx, sigma, mu):
        """gauss function: gauss(x, sigma=1, u=0)"""
        return np.exp(-(xx-mu)**2/(2.*sigma**2))

    new_x = x.copy()  # empty_like is buggy: see numpy trac
    new_x.fill(0.)
    if np.ma.is_masked(x):
        new_x.mask = False
    # do a loop over all elements
    for ii in np.arange(0,len(x)):
        # need some extra stuff for masked arrays
        # if np.ma.is_masked(t[ii]):
        #     raise ValueError('Masking the time dimension is not supported')
        #     #t_cur = t.data[ii]
        # else:
        #
        t_cur = t[ii]

        # find values which lie in the time range:
        t_ind = np.where(np.logical_and((t <= t_cur + delta_t), (t >= t_cur - delta_t)))[0]
        n_ind = len(t_ind)
        t_ind_u = t_ind[np.where(t_cur < t[t_ind])[0]]
        t_ind_l = t_ind[np.where(t_cur > t[t_ind])[0]]

        #pdb.set_trace()

        # use values at indices which have a symmetric unmasked sibling:
        #print '\nind = %i, t_ind = %s' % (ii, str(t_ind))
        t_ind_good = np.array([ii]) # dont care whether this is masked as there is no partner
        for jj in t_ind_l:
            if np.ma.is_masked(x[jj]):
                #print '--'
                continue # do nothing if x[jj] is masked
            else:
                t_jj_u = t[t_ind_u]

            # find corresponding t_ind_u, if it exists
            ind_tmp = t_ind_u[np.where(
                np.abs(t[jj]-t_cur - (-t_jj_u+t_cur)) < delta_t/n_ind  )[0]]
            if np.ma.is_masked(x[ind_tmp]):
                #print '++'
                continue # dicharge if masked
            # print ind_tmp

            if ind_tmp.shape[0] == 1:
                #print 'in', ind_tmp, t_ind, t_ind_l
                t_ind_good = np.concatenate((ind_tmp,t_ind_good))
                t_ind_good = np.concatenate((np.array([jj]),t_ind_good))
        t_ind_good.sort()
        # print t_ind_good

        weights = gauss(t[t_ind_good], delta_t/sigma_factor, t_cur)
        if np.ma.is_masked(x[t_ind_good]):
            weights[np.where(x[t_ind_good].mask)[0]] = np.ma.masked
        # print weights

        # if x.mask[ii]==False:
        #     pdb.set_trace()
        #pdb.set_trace()

        # normalize to one:
        weights = weights/weights.sum()
        # now calcutate value:
        new_x[ii] = (weights.reshape(weights.shape[0],1)* x[t_ind_good,...]).sum(axis=0)

    if row_vec:
        new_x = new_x.ravel()
    return new_x

def filter_gauss_conv(t, x, delta_t, sigma_factor=2.0):
    """ Filters a regularly-sampled signal with a Gaussian-weighted
    average with half-window length delta_t. The Gaussian bell is
    chosen such that at +/-delta_t it is valued at sigma_factor, e.g.
    sigma_factor=2 is c.a. 1/8 of the weight of the center. There
    is no phase shift.

    Warning: If x is masked, then NaN-values will be laundered into
    masked values in the output. This means that if x contains both a
    mask and NaNs, the output will only have a mask, extended over the
    NaNs.

    @param t: time vector
    @param x: data vector (or array)
    @param delta_t: filter time interval

    @note:
     - Performs filtering using a np.convolve.

    >>> t = np.linspace(0,100,10)
    >>> x = np.sin(t)
    >>> filter_gauss_conv(t, x, 10*np.pi)
    array([-0.35491283, -0.25778845, -0.06943494,  0.07441436,  0.03527717,
           -0.06628086, -0.05055887,  0.10013494,  0.20257496,  0.20875278])
    >>> tm = t.view(np.ma.MaskedArray)
    >>> xm = x.view(np.ma.MaskedArray)
    >>> xm[3] = np.ma.masked
    >>> filter_gauss_conv(tm, xm, 10*np.pi)
    masked_array(data = [-- -- -- 0.03802574496735643 0.035277166102035996 -0.06628086085499622
     -0.05055887469245819 0.10013493619377262 0.2025749575732381
     0.20875277572782974],
                 mask = [ True  True  True False False False False False False False],
           fill_value = 1e+20)
    <BLANKLINE>
    """
    delta_t = float(delta_t)

    # Make sure that dt < range(t)
    if delta_t >= (np.max(t)-np.min(t)) / 2.0:
        # I changed this to a ValueError, but it could go back to
        # TypeError if it's caught somewhere else -njw
        raise(ValueError(
            "delta_t is must be smaller than half the range of t"))
    if np.ma.is_masked(t):
        raise TypeError("Masking the time dimension is not supported")

    # Relate the sampling interval to dt
    # Use a mean just in case t isn't strictly regularly spaced
    halfwin = int(round( delta_t / np.diff(t).mean() ))

    # Get a kernel - just grap one from scipy
    kernel = gaussian(2*halfwin+1, halfwin/sigma_factor)
    kernel /= kernel.sum()

    # Deal with masked x
    if np.ma.is_masked(x):
        was_masked = True
        x_nan = x.data
        x_nan[x.mask is True] = np.nan
    else:
        was_masked = False
        x_nan = x

    # Interpolate over NaN values
    if True in np.isnan(x_nan):
        x_valid = x_nan[np.nonzero(np.isnan(x_nan)==False)]
        t_valid = t[np.nonzero(np.isnan(x_nan)==False)]
        x_interp = np.interp(t, t_valid, x_valid)
    else:
        x_interp = x_nan

    # Pad the data array
    pad0 = x[:halfwin].mean()
    padf = x[-halfwin:].mean()
    x_padded = np.hstack([pad0*np.ones(halfwin), x_interp, padf*np.ones(halfwin)])

    # Perform the filtering
    x_filtered = np.convolve(x_padded, kernel, mode='valid')

    if was_masked:
        # Build a new masked array around x_filtered in-place
        x_filtered = np.ma.masked_where(np.isnan(x_filtered),
                        x_filtered)

    return x_filtered

def filter_and_resample_slow(time, data, filter_window,
                             new_sample_int, first_sample=None):
    """Filters and resamples a time signal with a running average in a
    given time intervall +/-filter_window/2.  The filter has a PHASE
    SHIFT for a UN-equally sampled signal.  Accepts masked arrays but
    not "structured arrays" or "recarrays".

    If you have cython (or just build the modul) use the much faster
    filter_and_resample in helper_fns_cython.

    Having said this, this python implementation is probably really slow!

    @param time: the times of the timeseries
    @param data: the data of the timeseries
    @param filter_window: filter time interval
    @param new_sample_int: the new sampling interval (in units of time)
    @param first_sample: the time of the first sample in the resampled
                         signal. If None (default) it is
                         (approximatly) set to time[0]+filter_window

    @rtype: list of np.array
    @return: Returns [new_time, new_data]

    @note: 1) ignores any masks in the time variable.
           2) will not work if it finds no values within filter window
    @todo: Correct above point 2) (as well in helper_fns_cython.pyx)
    >>> tim = np.linspace(-3,90,1000); data = tim**2
    >>> filter_window = 30; new_sample_int = 30
    >>> filter_and_resample_slow(tim, data, filter_window, new_sample_int)
    (array([  0.,  30.,  60.,  90.]), array([   62.98183318,   975.90237234,  3678.22848073,  6826.19355542]))
    >>> data = np.ma.MaskedArray(data)
    >>> data[1:10] = np.ma.masked
    >>> filter_and_resample_slow(tim, data, filter_window, new_sample_int)
    (array([  0.,  30.,  60.,  90.]), array([   65.73049119,   975.90237234,  3678.22848073,  6826.19355542]))
    """
    filter_window = filter_window/2.
    if first_sample is None:
        first_sample = time[0] + filter_window
        # make it such that it lies nicely with the sampling interval
        first_sample = (np.round(first_sample/new_sample_int)) * new_sample_int
    new_t = np.arange( first_sample, time[-1]+1e-10*time[-1], new_sample_int)
    new_d = np.empty_like(new_t)
    ind1 = 0
    ind2 = 0
    for ii in range(len(new_t)):
        ind1 = get_ind_closest(time, new_t[ii]-filter_window, ind1)
        ind2 = get_ind_closest(time, new_t[ii]+filter_window, ind2)
        if ind1==-1 or ind2==-1:
            raise(ValueError('Index not found'))
        new_d[ii] = data[ind1:ind2+1].mean()

    return new_t, new_d



def get_ind_closest(vec, value, guess_ind=None):
    """
    Finds the index (ind) of vec such that vec[ind]-value is smallest.  Only
    works reliably for monotone vectors.  As start value it uses the
    value found in ind[0].

    This function is somewhat slower than get_ind_closest but can be
    called directly from Python.  (When calling from cython use
    get_ind_closest_faster)

    @param vec: the vector, usually a time
    @param value: the value which is compared to vec
    @param guess_ind: a guess for index around which the search
    (symmetric) will be started.

    @return: The number of indices away from

    @note: The algorithm is fairly dum, something like bisection might
    work much faster.

    >>> tim =np.linspace(-3,100,1000)
    >>> get_ind_closest(tim, 49.2)
    506
    >>> get_ind_closest(tim, 49.2, 400)
    506
    """
    # checks
    len_v = len(vec)
    if guess_ind < 0:
        guess_ind = 0
    if guess_ind>len_v-1:
        guess_ind = len_v-1
    # figure out sign of the difference between vec[guess_ind] and value
    si = np.int(np.sign(vec[guess_ind] - value))

    # do a while loop until the sign changes
    ii = 0
    while True:
        if guess_ind+ii<len_v:
            s1 = (np.sign(vec[guess_ind+ii] - value))
        if guess_ind-ii>=0:
            s2 = (np.sign(vec[guess_ind-ii] - value))
        if s1!=si:
            # break if sign changes
            ind = guess_ind + ii
            d1 = np.abs(vec[ind] - value)
            d2 = np.abs(vec[ind-1] - value)
            if d1<d2:
                return ind
            else:
                return ind-1
        if s2!=si:
            # break if sign changes
            ind = guess_ind - ii
            d1 = vec[ind] - value
            d2 = vec[ind+1] - value
            if d1<d2:
                return ind
            else:
                return ind+1
        if guess_ind+ii>=len_v and guess_ind-ii<0:
            if abs(guess_ind-len_v) < abs(guess_ind):
                return len_v-1
            else:
                return 0
        ii += 1

def spline_interpolation(vecs, t_par, smoothness=None, spline_order=3,
                         n_knots=None, n_knots_estimate=-1):
    """Finds a B-spline representation of an N-dimensional curve. Uses
    scipy.interpolate.fitpack.splprep.

    Given a list of N rank-1 arrays, vecs, which represent a curve in
    N-dimensional space parametrized by t_par, find a smooth
    approximating spline curve g(t_par). Uses the FORTRAN routine
    parcur from FITPACK.

    Example:
    t = np.hstack((np.linspace(0,4*np.pi, 2000), np.linspace(4*np.pi+0.1, 8*np.pi, 500)))
    xx = np.sin(t)
    x = xx + np.random.normal(scale=0.1, size=t.shape)
    fn,tckp = spline_interpolation([x],t, smoothness=40.0)
    plot(t,x),hold(True), plot(t, fn(t)[0],'r'), plot(t,xx, 'g')

    @param vecs: A list of sample vector arrays representing the curve.
    @param t_par: An array of parameter values (probably time).
    @param smoothness: smoothness parameter (default determinded by splprep)
    @parameter spline_order: spline order (default & reccomended 3)

    @note: adapted from
           http://www.scipy.org/Cookbook/Interpolation#head-34818696f8d7066bb3188495567dd776a451cf11
    """
    # find the knot points
    tckp,u = splprep(vecs, u=t_par, s=smoothness, k=spline_order,
                     nest=n_knots_estimate)

    # return a function which evaluates the spline
    return lambda tt : splev(tt, tckp), tckp



def polyval(p, x):
    """
    Copied from numpy and adjusted to work for masked arrays.

    Evaluate a polynomial at specific values.

    If `p` is of length N, this function returns the value:

    ``p[0]*x**(N-1) + p[1]*x**(N-2) + ... + p[N-2]*x + p[N-1]``

    If `x` is a sequence, then `p(x)` is returned for each element of `x`.
    If `x` is another polynomial then the composite polynomial `p(x(t))`
    is returned.

    Parameters
    ----------
    p : array_like or poly1d object
       1D array of polynomial coefficients (including coefficients equal
       to zero) from highest degree to the constant term, or an
       instance of poly1d.
    x : array_like or poly1d object
       A number, a 1D array of numbers, or an instance of poly1d, "at"
       which to evaluate `p`.

    Returns
    -------
    values : ndarray or poly1d
       If `x` is a poly1d instance, the result is the composition of the two
       polynomials, i.e., `x` is "substituted" in `p` and the simplified
       result is returned. In addition, the type of `x` - array_like or
       poly1d - governs the type of the output: `x` array_like => `values`
       array_like, `x` a poly1d object => `values` is also.

    See Also
    --------
    poly1d: A polynomial class.

    Notes
    -----
    Horner's scheme [1]_ is used to evaluate the polynomial. Even so,
    for polynomials of high degree the values may be inaccurate due to
    rounding errors. Use carefully.

    References
    ----------
    .. [1] I. N. Bronshtein, K. A. Semendyayev, and K. A. Hirsch (Eng.
       trans. Ed.), *Handbook of Mathematics*, New York, Van Nostrand
       Reinhold Co., 1985, pg. 720.

    Examples
    --------
    >>> np.polyval([3,0,1], 5)  # 3 * 5**2 + 0 * 5**1 + 1
    76
    >>> np.polyval([3,0,1], np.poly1d(5))
    poly1d([ 76.])
    >>> np.polyval(np.poly1d([3,0,1]), 5)
    76
    >>> np.polyval(np.poly1d([3,0,1]), np.poly1d(5))
    poly1d([ 76.])

    """
    p = np.asarray(p)
    if isinstance(x, np.poly1d):
        y = 0
    else:
        y = np.zeros_like(x)
    for i in range(len(p)):
        y = x * y + p[i]
    return y

def get_current_fieldpy_git_hash():
    """
    Returns the has of the current git revision of the fieldpy
    package.

    @return: Git SHA1 hash
    """
    dir_of_this_file = os.path.dirname(__file__)
    return get_current_git_hash(dir_of_this_file)

def get_current_git_hash(dirname):
    """
    Returns the has of the current git revision of directory passed in.

    @param dirname: directory in which the git revision is queried

    @return: Git SHA1 hash    
    """
    git_cmd = 'git rev-parse --verify HEAD'
    cmd_str = 'cd ' + dirname +';' + git_cmd  # cd into directory of this file
    try:
#        return subprocess.check_output(cmd_str, shell=True).strip()
        return _check_output(cmd_str, shell=True).strip()
    except subprocess.CalledProcessError as err:
        return 'git version control not running or producing error: no current version'
    # except:
    #     return 'other error processing git'


def _check_output(*popenargs, **kwargs):
    r"""
    Copied from python 2.7 standard libary as python 2.6 doesn't have
    it.  Remove after Flavien updated to 2.7.
    
    Run command with arguments and return its output as a byte string.

    If the exit code was non-zero it raises a CalledProcessError.  The
    CalledProcessError object will have the return code in the returncode
    attribute and output in the output attribute.

    The arguments are the same as for the Popen constructor.  Example:

    >>> _check_output(["ls", "-l", "/dev/null"]) # doctest:+ELLIPSIS
    'crw-rw-rw- 1 root root 1, ...

    The stdout argument is not allowed as it is used internally.
    To capture standard error in the result, use stderr=STDOUT.

    >>> _check_output(["/bin/sh", "-c",
    ...               "ls -l non_existent_file ; exit 0"],
    ...              stderr=subprocess.STDOUT)
    'ls: cannot access non_existent_file: No such file or directory\n'
    """
    if 'stdout' in kwargs:
        raise ValueError('stdout argument not allowed, it will be overridden.')
    process = subprocess.Popen(stdout=subprocess.PIPE, *popenargs, **kwargs)
    output, unused_err = process.communicate()
    retcode = process.poll()
    if retcode:
        cmd = kwargs.get("args")
        if cmd is None:
            cmd = popenargs[0]
        raise subprocess.CalledProcessError(retcode, cmd, output=output)
    return output

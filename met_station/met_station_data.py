"""
This file contains classes to hold the logged data not directly related to the stream site:
 - met station data

Nothing fancy is done here, as all this is done in 

"""
import numpy as np
import datetime
import pylab as pl
import matplotlib.mlab as mlab
from scipy import polyval, polyfit

from fieldpy.core.experiment_classes import *
import fieldpy.core.helper_fns as helper_fns
from fieldpy.stream_gauging.calibration import SingleCalibration


#Attempt to include metstation data in the code so that they can be plotted in correlation with the discharge.
class MS_data(CampbellCr1000):
    """
    Class to hold and process Met station data.

    self.data will have the following 
    """
    def __init__(self, filenames,given_headers=[]):
        #to be modified once it is working and change the 4 last lines of comment
        """
        Class to hold and process Met station data.

        @type filenames: list of strings
        @param filenames: a list of TOA5 Campbell Cr1000 files in
                          chronological order with the same data format.

                          
        @type given_headers: list of stings
        @param given_headers: A list of header strings to be used instead
                              of the ones given in the file header.

        @note:
         - Error checking is just marginal.
         -  Masking record arrays seems a bit tricky here is what I found:
           - U{http://thread.gmane.org/gmane.comp.python.numeric.general/34100/focus=34125}
           - but third creation method here seems to work:
             U{http://docs.scipy.org/doc/numpy/reference/maskedarray.generic.html#using-numpy-ma}

        """

        super(MS_data, self).__init__(filenames, given_headers)
        self.check()

    def check(self):
        super(MS_data, self).check()   

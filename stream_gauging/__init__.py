"""The sub-module stream_gauging is used to do all things stream gauging:
 1. stage measurements
 2. conductivity measurements
 3. conductivity sensor calibration
 4. dilution gauging
 5. stage-discharge relation
 6. discharge time series

0 Example
=========
  Probably it is easiest to go by an example which is located in the
  folder fieldpy/stream_gauging/example_stream_site_2010/.  There look
  through the script in analyse_it.py which follows the layout below.
  If you like it, then the best way of using it is to copy the
  contents of the fieldpy/stream_gauging/template/ folder to where you
  have your data and edit it accordingly.

1 Stage measurements
====================
  The stage measurements are done by the SR50 ultrasonic depth gauge.

  B{Workflow stage measurements:}

    1. Create a L{logged_data.Stage} instance by::
       st = Stage([filenames_of_TOA5_file])
    2. plot it with L{logged_data.Stage.plot_date}
    3. Filter it by running L{logged_data.Stage.apply_all_filters}

2 Conductivity measurements
===========================
  The conductivity measurements are done by Garry Clarke's home made
  conductivity sensor.  Note that the sensor reading is not the actual
  conductivity but the ratio between measured and excitation voltage.

  B{Workflow conductivity measurements:}

    1. Create a L{logged_data.Conductivity} instance by::
       ec = Conductivity([filenames_of_TOA5_file])
    2. plot it with L{logged_data.Conductivity.plot_date}
    3. Filter it by running L{logged_data.Conductivity.apply_all_filters}


3 Conductivity sensor calibration
=================================
  This is handled by the classes in L{calibration}.

  B{Workflow calibration:}

    1. For each calibration make a .maw file like e.g.::
        #maw 1st Conductivity Calibration 13 July 2010
        #
        # Calibration of the older conductivity sensor in the proglacial river of
        # GL1 on 13 July at 10:30.
        #
        # Note: Times in calibration are not accurate.
        #
        # Calibration was done by premixing 20g NaCl in 2L of water, to get a 10g/L solution.
        # This solution was then added into a 5000mL bucket of water, in the steps written below.
        #
        # Metadata:
        #metadata.experimenter = 'MAW + UM'
        #metadata.bucket_size = 5.0
        #metadata.bucket_size_units = 'l'
        #metadata.calibaration_solution_concentration = 10.0
        #metadata.calibaration_solution_concentration_units = 'g/l'
        #metadata.sensor = 'older Garry sensor'
        #metadata.reference_resistor = 10e3
        #metadata.reference_resistor_units = 'Ohm'
        #metadata.note = 'Times in calibration are not accurate'
        #
        # Format:
        # time (UTC-7) [time_str], calibration_solution (ml) [float], sensor_readout () [float]
        "2010-07-13 10:30:00",0,    0.3030
        "2010-07-13 10:31:00",1.25, 0.2965
        "2010-07-13 10:32:00",2.5,  0.2890
        "2010-07-13 10:33:00",3.75, 0.2815
        "2010-07-13 10:34:00",5,    0.2730
        "2010-07-13 10:35:00",7.5,  0.2595
        "2010-07-13 10:36:00",10,   0.2500
        "2010-07-13 10:37:00",15,   0.2320
        "2010-07-13 10:38:00",20,   0.2175
        "2010-07-13 10:39:00",25,   0.2055
        "2010-07-13 10:40:00",30,   0.1945
        "2010-07-13 10:41:00",35,   0.1860
        "2010-07-13 10:42:00",40,   0.1760
        "2010-07-13 10:43:00",45,   0.1685
        "2010-07-13 10:44:00",50,   0.1620

    2. Create an L{calibration.AllCalibrations} instance which holds
       all the calibrations of a season::
        ac = AllCalibartions(list_of_calibration_files)
    3. Check that all the calibrations are suitably similar by:
       - check the output of the program and see whether the slopes are similar
         (the 'a' in the printout)
       - plot all calibrations with
         L{calibration.AllCalibrations.plot_all} and see if they look
         good.  It's ok for the lines to be offset but the slope
         should be identical-ish.
    
4 Dilution gauging
==================
  The dilution gauging will consist of individual salt injections
  recorded as conductivity change of the water. This is translated
  into a concentration with the help of the calibration. Then the
  concentration is related to the discharge via:
  
  M / S{int} c dt = Q

  where M is the amount of injected salt, c is the concentration and Q
  is the discharge.

  B{Workflow dilution gauging:}
    1. have the stage, conductivity and calibrations set-up as above
    2. Create a .maw file listing all salt injections with the
       following format::
        #maw Dilution gauging injections from 13 July 2010
        #
        # Metadata:
        #metadata.experimenter = 'MAW + UM'
        #metadata.injection_point = "MAW's injection point"
        #metadata.sensor = 'older Garry sensor'
        #
        # Format:
        # time (UTC-7) [time_str], amount (kg) [float]
        "2010-07-13 09:07:00", 0.2
        "2010-07-13 09:22:45", 0.4
        "2010-07-13 11:17:00", 0.4
        "2010-07-13 13:16:00", 0.4
        "2010-07-13 13:16:00", 0.4

    3. create a L{dilution_gauging.DilutionGauging} instance like so::
        dilg = DilutionGauging(injections_maw_file, (0,150), ec, ac, st)
    4. Check that you're happy with the windows chosen for the injections::
        dilg.plot_all()
    5. Make the stage-discharge relation for the day::
        dilg.make_stage_discharge_relation(verbose_=True, order_of_poly=2)

5 Stage-discharge relation
==========================

  With the L{dilution_gauging.DilutionGauging} we created a
  stage-discharge relation for one day of dilution-gauging
  experiments.  But presumably more than one day of dilution gauging
  was done, so these need to be combined to lead to a stage-discharge
  relation for the whole season.  Which is done with the
  L{stage2discharge.Stage2Discharge} class.

  B{Workflow stage-discharge relation:}
    1. have the stage, conductivity, calibrations and dilution-gauging
       set-up as above.
    2. make a L{stage2discharge.Stage2Discharge} instance::
        s2d = stage2discharge.Stage2Discharge(xxx)
    3. have a look how the individual stage-discharge relations compare::
        s2d.plot_all()

6 Discharge time series
=======================
  Now, finally we are in a position to convert the stage records into
  a discharge time series.

  B{Workflow discharge time series:}
    1. have the stage, conductivity, calibrations, dilution-gauging
       and stage-discharge relation made as above.

    2. tell your L{logged_data.Stage} instance how to convert the
       stage into a discharge::
        st.set_stage2discharge_method(s2d.stage2discharge)
       note you can choose here from different methods (once
       implemented):
        - L{stage2discharge.Stage2Discharge.stage2dis_average}
    3. convert the stage::
        st.make_discharge()
    4. admire (and check) it:
        st.plot_date('discharge')
    5. THE END
  

"""
__all__ = ['calibration', 'dilution_gauging', "stream_gauging"]

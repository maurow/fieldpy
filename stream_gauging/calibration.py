"""
This file contains the functions to read calibration files.

Calibration file format is almost identical to TOA5:
"TIMESTAMP","CONCENTRATION","SENSOR_EC_READING"
"2010-07-13 08:50:00",0.00,0.234
"""
import numpy as np
import datetime
import pylab as plt
import matplotlib.mlab as mlab


from fieldpy.core.experiment_classes import *
from fieldpy.core import raw_file_readers

class AllCalibrations(ExperimentData):
    """
    This class can hold several calibrations and provides the
    following functions to convert sensor readout to concentration:

     - L{readout2concentration_closest}
     - L{readout2concentration_weighted}
    """
    def __init__(self, list_of_filenames, plot_yes=False):
        """Initialises the class with

        @type list_of_filenames: list of strings
        @param list_of_filenames: The file names of the calibration files in .maw file format

        @type plot_yes: boolean
        @param plot_yes: Plot the linear regression if True (default = False)

        >>> from fieldpy.stream_gauging.calibration import *
        >>> dir_ = '../stream_gauging/test_data/'
        >>> fielns = ['calibration_13JUL10.maw', 'calibration_18JUL10_1.maw', 'calibration_18JUL10_2.maw']
        >>> ac = AllCalibrations([dir_ + fl for fl in fielns])
        Linear regression of conductance vs concentration
        a=345.72 b=-0.08, root mean squared error= 0.000, R^2 = 0.997
        Linear regression of conductance vs concentration
        a=363.19 b=-0.10, root mean squared error= 0.001, R^2 = 0.996
        Linear regression of conductance vs concentration
        a=254.78 b=-0.03, root mean squared error= 0.000, R^2 = 0.999
        """
        super(AllCalibrations, self).__init__()
        self.calibrations = []
        self._plot_yes = plot_yes
        for filename in list_of_filenames:
            self.calibrations.append(SingleCalibration(filename, self._plot_yes))
        # sort them according to when the calibartion was done
        cmpfn = lambda x, y : cmp(x._datetime, y._datetime)
        self.calibrations.sort(cmp=cmpfn)
        self.check()

    def __repr__(self):
        st = 'AllCalibrations instance containing:\n'
        for ca in self.calibrations:
            st = st + ca.__str__() + '\n'
        return st[:-1]
    def check(self):
        """
        Does integrity checks:
         - check that all SingleCalibration.md.reference_resistor are the same
        """
        super(AllCalibrations, self).check()
        ref_r = self.calibrations[0].md.reference_resistor
        for cali in self.calibrations[1:]:
            if cali.md.reference_resistor!=ref_r:
                raise ValueError('The reference resistors of the individual calibrations are not equal!')
    def plot_all(self):
        """
        Plots all calibrations.
        """
        axs = self.calibrations[0].plot_it()
        axs[0].hold(True)
        axs[1].hold(True)
        for ca in self.calibrations[1:]:
            ca.plot_it(axs)
        axs[0].set_title('Linear Regression between conductance and concentration')

    def conductance2concentration_average(self, times, conductance,
                                          background_cond, verbose_=False):
        """
        Converts the conductance into a concentration using an average of
        the calibartions.

        @type times: numpy array matplotlib time stamps
        @param times: times of sensor readout

        @type conductance: numpy array
        @param conductance: sensor readouts converted to conductance

        @type background_cond: float
        @param background_cond: the conductance at zero salt concentration

        @type verbose_: boolean
        @param verbose_: print which calibration was used

        @rtype: numpy array or float
        @return: concentration
        """
        # average the self.calibrations.md.ar coefficient
        ar_aver = 0
        for caa in self.calibrations:
            ar_aver += caa.md.ar
        ar_aver = ar_aver/len(self.calibrations)
        cali2use = SingleCalibration(None)
        cali2use.md.ar = ar_aver
        cali2use.md.reference_resistor = self.calibrations[0].md.reference_resistor
        if verbose_:
            print 'Used slope is %f' % ar_aver
        return cali2use.conductance2concentration(conductance, background_cond)


    def readout2concentration_average(self, times, readouts, background_readout, verbose_=False):
        """
        Converts the readout into a concentration using an average of
        the calibartions.

        @type times: numpy array matplotlib time stamps
        @param times: times of sensor readout

        @type readouts: numpy array
        @param readouts: sensor readouts

        @type background_readout: float
        @param background_readout: the sensor readout at zero salt concentration (default=1)

        @type verbose_: boolean
        @param verbose_: print which calibration was used

        @rtype: numpy array or float
        @return: concentration
        """
        # average the self.calibrations.md.ar coefficient
        ar_aver = 0
        for caa in self.calibrations:
            ar_aver += caa.md.ar
        ar_aver = ar_aver/len(self.calibrations)
        cali2use = SingleCalibration(None)
        cali2use.md.ar = ar_aver
        cali2use.md.reference_resistor = self.calibrations[0].md.reference_resistor
        if verbose_:
            print 'Used slope is %f' % ar_aver
        return cali2use.readout2concentration(readouts, background_readout)

    def readout2concentration_closest(self, times, readouts, background_readout, verbose_=False):
        """
        Converts the readout into a concentration using the
        calibration closest in time to the readouts (note only the
        time of first readout is checked).

        @type times: numpy array matplotlib time stamps
        @param times: times of sensor readout

        @type readouts: numpy array
        @param readouts: sensor readouts

        @type background_readout: float
        @param background_readout: the sensor readout at zero salt concentration (default=1)

        @type verbose_: boolean
        @param verbose_: print which calibration was used

        @rtype: numpy array or float
        @return: concentration
        """
        # figure out which calibration to use
        dt = [np.abs(times[0]-ca.data['time'][0]) for ca in self.calibrations]
        cali2use = self.calibrations[np.argmin(dt)]
        if verbose_:
            print 'Used calibration: ' + cali2use.__repr__() + '\nwith slope ' + cali2use.md.ar.__str__()
        return cali2use.readout2concentration(readouts, background_readout)
    def conductance2concentration_closest(self, times, readouts, background_readout, verbose_=False):
        """
        Converts the readout into a concentration using the
        calibration closest in time to the readouts (note only the
        time of first readout is checked).

        @type times: numpy array matplotlib time stamps
        @param times: times of sensor readout

        @type readouts: numpy array
        @param readouts: sensor readouts

        @type background_readout: float
        @param background_readout: the sensor readout at zero salt concentration (default=1)

        @type verbose_: boolean
        @param verbose_: print which calibration was used

        @rtype: numpy array or float
        @return: concentration
        """
        # figure out which calibration to use
        dt = [np.abs(times[0]-ca.data['time'][0]) for ca in self.calibrations]
        cali2use = self.calibrations[np.argmin(dt)]
        if verbose_:
            print 'Used calibration: ' + cali2use.__repr__() + '\nwith slope ' + cali2use.md.ar.__str__()
        return cali2use.conductance2concentration(readouts, background_readout)

    def readout2concentration_weighted(self, times, readouts, background_readout):
        """
        Converts the readout into a concentration using the weighted
        calibration of the two calibrations closest in time to the readout.

        @type times: numpy array
        @param times: times of sensor readout

        @type readouts: numpy array
        @param readouts: sensor readouts

        @type background_readout: float
        @param background_readout: the sensor readout at zero salt concentration (default=1)

        @rtype: numpy array or float
        @return: concentration
        """
        pass



class SingleCalibration(LoggedData):
    """
    This class holds all the methods to read and process the salt
    calibation of the electical conductivity (EC) sensor.
    """
    def __init__(self, filename=None, plot_yes=True):
        """Initialises the class.  If filename is set to None, the
        class needs to be initialised by hand.

        @type filename: string
        @param filename: The file name of the calibration file in .maw file format

        @type plot_yes: boolean
        @param plot_yes: Plot the linear regression if True (default)

        >>> from fieldpy.stream_gauging.calibration import *
        >>> sc = SingleCalibration('../stream_gauging/test_data/calibration_13JUL10.maw', plot_yes=False)
        Linear regression of conductance vs concentration
        a=345.72 b=-0.08, root mean squared error= 0.000, R^2 = 0.997
        >>> sc
        Conductivity sensor calibration at 2010-07-13 10:30
        """
        super(SingleCalibration, self).__init__()
        if filename is not None:
            self.filename = filename
            self.data, self.raw_data, self.md = raw_file_readers.read_maw_file(filename)
            self._datetime = plt.num2date(self.data['time'][0])
            self._plot_yes = plot_yes
            self.make_calibration()
            self.check()
        else:
            return

    def __repr__(self):
        try:
            return 'Conductivity sensor calibration at %s' % datetime.datetime.strftime(self._datetime, '%Y-%m-%d %H:%M')
        except:
            return 'Conductivity sensor calibration at no time'
    def __str__(self):
        try:
            return 'Conductivity sensor calibration at %s' % datetime.datetime.strftime(self._datetime, '%Y-%m-%d %H:%M')
        except:
            return 'Conductivity sensor calibration at no time'

    def readout2conductance(self, readout):
        """
        Calcualtes the conductance across the sensor electrodes for a
        given sensor readout.

        @type readout: float or numpy array of floats
        @param readout: the sensor readout

        @rtype: numpy array or float
        @return: conductance
        """
        return (1-readout)/(self.md.reference_resistor*readout)

    def readout2concentration(self, readout, background_readout=None):
        """
        Function which calculates the concentration for a given sensor
        readout using the least square line fitted to the
        conductance-concentration relation.  If a background_readout
        is given then this is take into consideration.

        @type readout: float or numpy array of floats
        @param readout: the sensor readout

        @type background_readout: float
        @param background_readout: the sensor readout at zero salt concentration (default=1)

        @rtype: numpy array or float
        @return: concentration
        """
        cond = self.readout2conductance(readout)
        if background_readout is None:
            background = None
        else:
            background = self.readout2conductance(background_readout)
        return self.conductance2concentration(cond, background)

    def conductance2concentration(self, conductance, background=None):
        """
        """
        if background is None:
            return np.polyval([self.md.ar, self.md.br], conductance)
        else:
            return np.polyval([self.md.ar, 0], conductance - background)

    def make_calibration(self):
        """
        Makes the calibaration, afterwards the the method
        L{readout2concentration} is available to be used.

        @rtype: dict: {function, float, float, float, float}
        @return: A dictionary containing:
                 - 'a': a in ax+b
                 - 'b': b in ax+b
                 - 'R_squared': R^2
                 - 'rmse': root mean squared error'
        """

        # check that the right units are used
        if (    (not self.md.calibaration_solution_concentration_units=='g/l')
            and (not self.md.calibaration_solution_concentration_units=='gl^-1')):
            raise TypeError("self.md.calibaration_solution_concentration_units is not 'g/l' or 'gl^-1' but '%s'"
                            % self.md.calibaration_solution_concentration_units)
        if not self.md.bucket_size_units=='l':
            raise TypeError("self.md.bucket_size_units is not 'l' but '%s'"
                            % self.md.bucket_size_units)
        if not self.md.units[1]=='ml':
            raise TypeError("self.data['calibaration solution'] (i.e. self.md.units[1]) is not 'ml' but '%s'"
                            % self.md.units[1])

        # calcualte concentration in bucket
        amount_of_salt = self.data['calibration_solution']/1e3*self.md.calibaration_solution_concentration/1e3 # amount of salt in bucket in [kg]
        bucket_size_m3 = self.md.bucket_size/1e3 # bucket size in m^3
        concentration = amount_of_salt/bucket_size_m3 # concentration in kg/m^3
        self.data = mlab.rec_append_fields(self.data, 'concentration', concentration)

        # convert sensor reading (which is a resistance ration) into
        # conductance with the formula cond = 1/(Rf * (reading - 1))
        sensor_readout = self.data['sensor_readout']
        conductance_in_sensor = self.readout2conductance(sensor_readout)
        self.data = mlab.rec_append_fields(self.data, 'conductance_in_sensor', conductance_in_sensor)

        # fit a straight line by least squares
        n_obs = len(concentration)
        (ar, br) = np.polyfit(conductance_in_sensor, concentration, 1)
        # ar,residuals, rank, singular_values, rcond  = np.polyfit(conductance_in_sensor, concentration, 1, full=True)
        # print ar, residuals, rank, singular_values, rcond
        # br = ar[1]
        # ar = ar[0]
        predicted_concentration = np.polyval([ar,br], conductance_in_sensor)
        #compute the squared error
        mean_squared_error = sum((predicted_concentration-concentration)**2)
        # root mean squared error
        rmse = np.sqrt(mean_squared_error)/n_obs
        # coefficient of determination R^2
        mean_concentration = sum(concentration)/n_obs
        Stot = sum((concentration - mean_concentration)**2)
        Serr = sum((predicted_concentration - concentration)**2)
        R_squared = 1 - Serr/Stot
        print('Linear regression of conductance vs concentration')
        print('a=%.2f b=%.2f, root mean squared error= %.3f, R^2 = %.3f' % (ar, br, rmse, R_squared))

        self.md.ar = ar
        self.md.br = br
        self.md.R_squared = R_squared
        self.md.rmse = rmse


        # plot it
        if self._plot_yes:
            self.plot_it()

    def plot_it(self, axs=None):
        """
        Plot the calibration curve.

        @type axs: list of matplotlib axes object
        @param axs: two matplotlib axes in which to plot (default None: make a new ones).

        @rtype: list of matplotlib axes object
        @returns: list of the used matplotlib axes
        """
        # calc all the needed quantities
        conductance_in_sensor = self.data['conductance_in_sensor']
        sensor_readout = self.data['sensor_readout']
        concentration = self.data['concentration']
        predicted_concentration = np.polyval([self.md.ar,self.md.br], conductance_in_sensor)
        sr = np.linspace(sensor_readout[-1]-0.05,sensor_readout[0]+0.05, 50)
        fnval = self.readout2concentration(sr)

        # plot linear regression
        if axs is None:
            axs = []
            axs.append(plt.subplot(2,1,1))
            axs.append(plt.subplot(2,1,2))
        axs[0].yaxis.grid(True)
        axs[0].set_title('Linear Regression between conductance and concentration: a=%.2f b=%.2f, root mean squared error= %.3f, R2 = %.3f'
                 % (self.md.ar, self.md.br, self.md.rmse, self.md.R_squared))
        axs[0].plot(conductance_in_sensor, predicted_concentration,'r')
        axs[0].plot(conductance_in_sensor, concentration,'kx')
        if np.isnan(self.md.reference_resistor):
            axs[0].set_xlabel('Conductance (arbitrary units)')
        else:
            axs[0].set_xlabel('Conductance (1/%s)' % self.md.reference_resistor_units)
        axs[0].set_ylabel('Concentration (kg/m3)')
        axs[0].legend(['regression','data'], loc='best')

        # plot sensor vs concentration
        axs[1].yaxis.grid(True)
        axs[1].set_title('Sensor readout vs concentration')
        axs[1].plot(sr, fnval,'r')
        axs[1].plot(sensor_readout, predicted_concentration,'.r')
        axs[1].plot(sensor_readout, concentration,'kx')
        axs[1].legend(['regression','data'], loc='best')
        axs[1].set_xlabel('Sensor readout ()')
        axs[1].set_ylabel('Concentration (kg/m3)')

        return axs
